FROM python:3.9.2-slim

ARG GIT_COMMIT
ARG BRANCH
ENV GIT_COMMIT=${GIT_COMMIT}
ENV GIT_BRANCH=${BRANCH}
LABEL GIT_COMMIT==$GIT_COMMIT
LABEL GIT_BRANCH=${BRANCH}

COPY /src /app
WORKDIR /app
RUN python -m pip install -r ./requirements.txt
CMD ["python","./main.py"]