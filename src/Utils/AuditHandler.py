import datetime
import json

class AuditHandler(object):
    def __init__(self, request, db_client):
        self.db_client = db_client
        self.request = request
        self.user_id = 0

    def create(self, table, data, auditable_id=None):
        self.db_client.key_value_insert(table, data)
        if auditable_id is None:
            auditable_id = self.db_client.last_insert_id()
        self._save_audit(table, 'created', auditable_id, data, {})
        return auditable_id

    def update(self, table, new_data, id_key):
        old_data = self._get_entity_with_old_fields(table,  new_data, id_key)
        self.db_client.key_value_update(table, new_data, id_key)
        self._save_audit(table, 'updated', new_data[id_key], new_data, old_data)

    def delete(self, table, data, id_val, id_key):
        old_data = self._get_entity_with_old_fields(table,  data, id_key)
        query = "DELETE FROM {table} WHERE {field} = {val}".format(
            table=table,
            field=id_key,
            val=id_val
        )

        self.db_client.command(query)
        self.db_client.commit()
        self._save_audit(table, 'deleted', id_val, {}, old_data)

    def _get_entity_with_old_fields(self, table, data, id_key):
        fields = data.keys()
        query = "SELECT {keys} FROM {table} WHERE {id_key} = '{id_value}'".format(
            keys=','.join(fields),
            table=table,
            id_key=id_key,
            id_value=data[id_key]
        )

        items = self.db_client.list_query(query, fields)
        if len(items) > 0:
            return items[0]
        else:
            return {}

    def _save_audit(self, table, event, auditable_id, new_data, old_data):
        auditable_type = table.split('.')[-1]
        remote_ip = self.request.headers.get("X-Real-IP") or \
            self.request.headers.get("X-Forwarded-For") or \
            self.request.remote_ip

        try:
            self.user_id = str(self.request.headers.get('X-USER-IDENTIFY') or '0')
        except:
            self.user_id = '0'

        self.db_client.key_value_insert('audits', {
            'user_id': self.user_id,
            'event': event,
            'auditable_type': auditable_type,
            'auditable_id': auditable_id,
            'url': self.request.uri,
            'ip_address': str(remote_ip),
            'user_agent': self.request.headers.get("User-Agent", ''),
            'old_values': json.dumps(old_data, indent=4, sort_keys=True, default=str),
            'new_values': json.dumps(new_data, indent=4, sort_keys=True, default=str),
            'created_at': datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S"),
            'updated_at': datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")
        })