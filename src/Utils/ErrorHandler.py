from functools import wraps
import traceback
import logging
import json
import sys


def handle_error():
    def wrapper(f):
        @wraps(f)
        def wrapped(self, *f_args, **f_kwargs):
            try:
                f(self, *f_args, **f_kwargs)
            except Exception as e:
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                self.set_status(500)
                self.write(json.dumps({"message": "Error"}))

        return wrapped

    return wrapper