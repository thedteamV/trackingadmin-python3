import math


class Pagination(object):
    def __init__(self, total_data, current_page, page_size):
        self.current_page = current_page
        self.page_size = page_size
        self.offset = self.current_page * self.page_size - self.page_size
        t_pages = float(total_data[0]['total']) / page_size
        self.total_pages = int(math.ceil(t_pages))
        self.total_items = total_data[0]['total']

    def get_current_page(self):
        return self.current_page

    def get_total_items(self):
        return self.total_items

    def get_offset(self):
        return self.offset

    def get_total_pages(self):
        return self.total_pages
