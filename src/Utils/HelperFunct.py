import json
import datetime, time
import uuid
import logging
import time
from datetime import datetime
from datetime import timedelta  

class HelperFunct():
    def setRequestID(self):
        """ Set RequestID for request. """
        request_id = uuid.uuid4()
        
        return request_id

    def setTrksysidArray_byTrkSysId (self, sql_client, db_conf):
        """ set array for all trk_sys_id (any route) """

        trksys_arr = {}
        try:       
            trk_inc_com = db_conf['sql_set_trksysid_all']
            sql_client.command(trk_inc_com)
            trksys_list_res = sql_client.get_results()
            for trksys_params in trksys_list_res:
                trksys_id = trksys_params[0]
                trksys_arr[trksys_id] = {}

                trksys_arr[trksys_id]['trk_sys_id'] = trksys_params[0]
                trksys_arr[trksys_id]['inc_affiliates'] = trksys_params[1]
                trksys_arr[trksys_id]['inc_offers'] = trksys_params[2]
                trksys_arr[trksys_id]['route'] = trksys_params[3]
        except Exception as e:
            logging.error(str(e))
            logging.error('HelperFunct.setTrksysidArray_byTrkSysId: Cannot build trk_sys array, reason: ' + str(e))

        return trksys_arr    

    def getParamFrom_db_parameters (self, sql_client, db_conf, param_name):
        """ get parameter value from data.db_parameters by param name """

        num_value = 0
        string_value = ""

        try:
            com_dbparam = db_conf['sql_select_db_param']
            com_dbparam = com_dbparam.format(param_name)
            sql_client.command(com_dbparam)
            result = sql_client.get_result()
            num_value = int(result[0])
            string_value = result[1]
        except Exception as e:
            logging.error(str(e))
            logging.error('getParamFrom_db_parameters: got error: ' + str(e))
        return num_value, string_value




    def insertConversionToQueue (self, click_id, conversion_id, transaction_id, track_route, db_conf, sql_client):
        """ insert conversion to crone queue for any purpose """

        click_data = ''
        comm = None
        job_id = db_conf['job_id_source_transaction_id']
        error_status = ''
        try:
            date_created = datetime.now()
            time_created = int(time.time()) 
            date_to_create = date_created + timedelta(minutes = db_conf['job_st_id_delay_min'])

            comm = db_conf['sql_insert_st_id_search_toqueue']
            comm = comm.format(job_id, click_id, conversion_id, transaction_id, track_route, date_to_create, date_created)
            sql_client.command(comm)
            sql_client.commit()
            if sql_client.mCursor.rowcount == 0:
                logging.error ('tr: ' + transaction_id + ': ' + track_route + '.helpFunct.insertConversionToQueue: adding to clicks_create_queue was failed for conv_id=' + str(conversion_id))
                error_status = 'conversion not inserted to queue st_id'
            else:
                logging.info ('tr: ' + transaction_id + ': ' + track_route + '.helpFunct.insertConversionToQueue: added to clicks_create_queue for conv_id=' + str(conversion_id))
        except Exception as e:
            logging.error(str(e))
            logging.warn('tr: ' + transaction_id + ': ' + track_route +'.helpFunct.insertConversionToQueue: Error to add conversion_id ' + conversion_id + ' to clicks queue: '  + str(e))
            error_status = 'exception on creating conversion to queue st_id'

        return error_status
