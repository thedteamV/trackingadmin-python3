from functools import wraps
import json


def validate_request(rules=None):
    def wrapper(f):
        @wraps(f)
        def wrapped(self, *f_args, **f_kwargs):
            error_messages = dict()
            is_valid = True

            try:
                body = json.loads(self.request.body)
            except:
                body = {}

            for key, rule in rules.items():
                value = None
                if rule['res'] == 'body' and key in body:
                    value = body[key]
                elif rule['res'] == 'query':
                    value = self.get_argument(key, default=None)
                elif rule['res'] == 'path' and key in self.path_kwargs:
                    value = self.path_kwargs[key]

                result, messages = validate(value, rule)

                if not result:
                    alias = key
                    if 'alias' in rule:
                        alias = rule['alias']
                    error_messages[alias] = messages
                    is_valid = False

            if not is_valid:
                self.set_status(400)
                self.write(json.dumps({"message": "Bad Request.", "errors": error_messages}))
            else:
                f(self, *f_args, **f_kwargs)

        return wrapped

    return wrapper


def validate(param_val, rules=None):
    result = True
    messages = []
    field_type = rules['type']

    if 'except' in rules and param_val in rules['except']:
        result = True
        messages = []
        return [result, messages]

    for rule_name, val in rules.items():
        if rule_name == 'min' and param_val is not None and not rule_min(field_type, param_val, val):
            messages.append('Min value is invalid')
            result = False
        elif rule_name == 'max' and param_val is not None and not rule_max(field_type, param_val, val):
            messages.append('Max value is invalid')
            result = False
        elif rule_name == 'required' and not rule_required(param_val):
            messages.append('The field is required')
            result = False
        elif rule_name == 'in' and param_val is not None and not rule_in(param_val, val):
            messages.append('The field is invalid')
            result = False
        elif rule_name == 'empty' and param_val is not None and not rule_empty(param_val, val):
            messages.append('The field is empty')
            result = False
    return [result, messages]


def rule_empty(param_val, val):
    if val is True:
        return True
    elif param_val == '':
        return False
    else:
        return True


def rule_in(param_val, val):
    if param_val is None:
        return False

    return param_val in val


def rule_min(filed_type, param_val, val):
    if param_val is None:
        return False

    if filed_type == 'int':
        return int(param_val) >= int(val)
    return True


def rule_max(filed_type, param_val, val):
    if param_val is None:
        return False

    if filed_type == 'int':
        return int(param_val) <= int(val)
    return True


def rule_required(val):
    return not (val is None)
