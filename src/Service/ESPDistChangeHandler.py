import sys
import time
import json
import logging
import traceback
from Service.Service import Service
import datetime

class ESPDistChangeHandler(Service):
    def __init__(self, name='ESPDistChangeHandler', data_config_file=None):
        super().__init__(name, self.worker, ['data_db'])

        self.sql_client = self.connection_hub.get("data_db")
        self.data_config_file = data_config_file
        if self.data_config_file is None:
            logging.error('%s can not find ESP config file: %s', self.name, self.name)
            exit()

        with open(self.data_config_file, 'rb') as data_json_file:
            self.data_config = json.load(data_json_file)
            data_json_file.close()

        self.interval = self.get_interval('esp_dist_changed_handler')
        logging.info('%s module is up: %s', self.name, self.name)

    def worker(self):
        logging.info('Starting %s worker: %s', self.name, self.name)

        while self.active:
            try:
                logging.info('System detects UI changes: %s', self.name)
                self.worker_func()
            except Exception as e:
                logging.error('Error running worker function.: %s', self.name)
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))

            time.sleep(self.interval)

    def worker_func(self):
        """ Detects UI changes on project to send notification to espDist thread. """
        param = self.get_ui_change_status()
        if param["NUM_VALUE"] != 0:
            logging.debug('System detected UI changes: %s', self.name)
            self.reset_ui_changes_status_param()
            logging.debug('Send notification to ESPDist thread: %s', self.name)
            queues = self.get_queues("detect_ui_change")
            queues[0].put("ui_change_detected")
        else:
            logging.debug('No UI changes found: %s', self.name)

    def get_ui_change_status(self):
        try:
            query = self.data_config['data_get_param_query']
            param = self.sql_client.dict_query(query, self.data_config['data_get_param_query_keys'])
            return param['email_dist_ui_status']
        except Exception as e:
            logging.error('Error getting data email_dist_ui_status param.: %s', self.name)
            logging.error(str(e))
            return dict()

    def reset_ui_changes_status_param(self):
        try:
            self.sql_client.key_value_update(
                'data.db_parameters',
                {'NAME': 'email_dist_ui_status', 'NUM_VALUE': 0, 'DATE_VALUE': datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")},
                "NAME"
            )
        except Exception as e:
            logging.error('Error reset data email_dist_ui_status param.: %s', self.name)
            logging.error(str(e))