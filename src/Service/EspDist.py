""" ESPDist.py """
import sys
import traceback
import time
import json
import logging
import random
import requests
import traceback
from tqdm import tqdm
from Service.Service import Service
from ESP.ListsManager import ListsManager
from ESP.PushTracker import PushTracker
import datetime
import re

class ESPDist(Service):
    """ ESPs Distribution class """

    def __init__(self, name='ESPDist', config_file=None, esp_config_file=None):
        """ Constructor. """
        super().__init__(name, self.worker, ["tracking_db", "data_db"])
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.esp_config = None
        self.sql_client = self.connection_hub.get("tracking_db")
        self.data_sql_client = self.connection_hub.get("data_db")

        if self.esp_config_file is None:
            logging.error('ESPDist can not find ESP config file: %s', self.name)
            exit()

        try:
            with open(self.esp_config_file, 'rb') as esp_json_file:
                self.esp_config = json.load(esp_json_file)
                esp_json_file.close()

            self.interval = self.get_interval('esp_dist_interval')
        except:
            logging.error('ESPDist could not connect to sql: %s', self.name)

        self.define_list_manager()
        logging.info('ESPDist module is up: %s', self.name)

    def define_list_manager(self):
        try:
            self.list_manager = ListsManager(name='ListsManager', sql_client=self.sql_client, config_file=self.config_file, esp_config_file=self.esp_config_file)
            self.list_manager.connect()
            logging.info('ESPDist created ListManager: %s', self.name)
        except Exception as e:
            logging.error('ESPDist could not create ListManager: %s', self.name)
            exit()

    def worker(self):
        """ Synchronized worker. """
        logging.info('---== Start ESP_SMSSP_Dist worker ==---')

        while self.active:
            try:
                self.worker_func()
                logging.info('---== Finish ESP_SMSSP_Dist worker ==---')
            except Exception as e:
                logging.error('Error running worker function.')
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))

            time.sleep(self.interval)

    def worker_func(self):
        """ Synchronized worker functionality. """
        self.check_queue_with_esp_ui_changes()

        logging.info('Validate and save new conversions to esp dist table: %s', self.name)
        count = self.save_valid_convs()

        if count == 0:
            return

        logging.info('Move validated conversions to queue: %s', self.name)
        self.move_convs_to_queue()

        logging.info('Send queued conversions to networks: %s', self.name)
        self.send_convs()

    def check_queue_with_esp_ui_changes(self):
        """ Check queue which contains notification about esp ui changes (from other thread)
        to update manager list. """
        queues = self.get_queues("detect_ui_change")
        if not queues[0].empty():
            logging.info('New notification that ui changes detected from espChangeHandler: %s', self.name)
            queues[0].get()
            self.define_list_manager()

    def save_valid_convs(self):
        new_convs_list = self.get_new_conversions()
        count_dist_convs = 0

        for conv in tqdm(new_convs_list, disable=True, desc="preparing new convs"):
            logging.info('Validate conversion: {} (email: {}, phone: {})'.format(conv['id'], conv['email'], conv['phone']))

            try:
                conv['status_valid_code'] = 0
                conv['status'] = 'New'
                email_check_res = True
                phone_check_res = True

                # Klean13 email check.
                if self.esp_config['klean13_active'] == True and conv['aff_id'] not in self.esp_config['klean13_affiliates_white_list']:
                    # Blacklists check.
                    if conv['aff_id'] in self.esp_config['klean13_affiliates_black_list'] or \
                        conv['offer_id'] in self.esp_config['klean13_offers_black_list']:
                        conv['status'] = 'Blacklisted'
                        email_check_res = False
                    else:
                        email_check_res = self.klean13_email_check(conv)

                if self.esp_config['everify_phone_active'] == True:
                    conv['phone'] = re.sub(r'[^\d]', '', conv['phone'])
                    phone_check_res = self.everify_phone_check(conv)

                if email_check_res is False:
                    logging.info('Validate conversion: {} (email is invalid)'.format(conv['id']))
                    conv['status_valid_code'] = 1

                if phone_check_res is False:
                    logging.info('Validate conversion: {} (phone is invalid)'.format(conv['id']))
                    conv['status_valid_code'] = 2

                if email_check_res or phone_check_res:
                    count_dist_convs = count_dist_convs + 1
                    logging.info('Validate conversion: {} (mark with status New)'.format(conv['id']))
                    conv['status'] = 'New'
                else:
                    conv['status_valid_code'] = 4
                    logging.info('Validate conversion: {} (unable to use conv to networks - email and phone both are invalid)'.format(conv['id']))
                    conv['status'] = 'EmailAndPhoneInvalid'

                self.insert_conv_record(conv)
            except:
                logging.error('Error inserting conversion:')
                logging.error(str(conv))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
        self.sql_client.commit()
        logging.info('Validated count of dist conversion: {}'.format(count_dist_convs))
        return count_dist_convs

    def move_convs_to_queue(self):
        """ Prepers new conversions for sending. """
        convs_inserted_num = 0
        convs_queued_num = 0

        # Gets the new conversions for queuing.
        new_convs_list = self.esp_config['new_convs_query']
        new_convs_list = self.sql_client.list_query(new_convs_list, self.esp_config['new_convs_query_keys'])

        # Queues the conversions.
        for new_conv in tqdm(new_convs_list, disable=True, desc="qeueing conversions"):
            logging.info('Move conversion to queue: {}'.format(new_conv['conversion_id']))

            try:
                delivery_types = []
                if new_conv['status_valid_code'] == 0  or new_conv['status_valid_code'] == 2:
                    delivery_types.append('email')
                if new_conv['status_valid_code'] == 0 or new_conv['status_valid_code'] == 1:
                    delivery_types.append('sms')

                logging.info('Move conversion to queue: {} (distribute to {} )'.format(new_conv['conversion_id'], delivery_types))
                dist_params_list = self.get_conv_dist_params(new_conv, delivery_types)

                for dist_params in dist_params_list:
                    try:
                        logging.info('{} distribute to list id {}'.format(new_conv['conversion_id'], dist_params['list_id']))
                        convs_queued_num += self.queue_new_conv(new_conv, dist_params)
                    except Exception as e:
                        logging.error('Error queuing conversion and dist params:')
                        logging.error(str(e))
                        (exc_type, exc_value, exc_traceback) = sys.exc_info()
                        logging.error(repr(traceback.format_tb(exc_traceback)))
                        logging.error('Conversion:' + str(new_conv))
                        logging.error('Dist params:' + str(dist_params))
            except Exception as e:
                logging.error('Error queuing conversion:')
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                logging.error('Conversion:' + str(new_conv))

        self.sql_client.commit()

        # Marks the queued conversions in the conversions table.
        self.mark_queued_convs_by_conversion_id(new_convs_list)

        # Prints log for the entire flow.
        logging.info('New conversions collected: {}'.format(len(new_convs_list)) + \
                     '  New conversions inserted: {}'.format(convs_inserted_num) + \
                     '  New conversions collected: ' + str(len(new_convs_list)) + \
                     '  New conversions queued: ' + str(convs_queued_num))

    def send_convs(self):
        """ Sends queued conversions to the networks. """
        # Collects the pending conversions.
        pending_convs_query = self.esp_config['pending_convs_query']
        pending_convs_query = pending_convs_query.replace('{time}', str(int(time.time())))
        pending_convs_query = pending_convs_query.replace('{conv_types}',
                                                              '\'Optin\',\'Lead\',\'Deposit\',\'FTD\',\'Event\'')
        pending_convs_query += "AND  `esp_dist`.`dist_queue`.`list_id` IN ( "
        pending_convs_query += ",".join(self.list_manager.esp_lists.keys())
        pending_convs_query += ") order by `esp_dist`.`dist_queue`.`time_to_send` asc  LIMIT 800"
        convs_list = self.sql_client.list_query(pending_convs_query, self.esp_config['pending_convs_query_fields'])
        #logging.info('Pending conversions found: %s', str(len(convs_list)))
        #logging.info('NetworkID lists found: %s', str(self.list_manager.esp_lists.keys()))

        self.list_manager.add_contacts(convs_list)

        # Dequeues the sent conversions.
        try:
            if len(convs_list) > 0:
                queue_ids = ''
                for conv in convs_list:
                    queue_ids += '\'' + str(conv['queue_id']) + '\','
                queue_ids = queue_ids[:-1]

                dequeue_convs_query = self.esp_config['dequeue_sent_convs_query']
                dequeue_convs_query = dequeue_convs_query.replace('{time_sent}', str(int(time.time())))
                dequeue_convs_query = dequeue_convs_query.replace('{queue_ids}', queue_ids)
                self.sql_client.command(dequeue_convs_query)
                self.sql_client.commit()
        except:
            logging.info('Error dequeueing sent conversions')

    def get_new_conversions(self):
        """ Returns a list of the new conversions from the DB. """
        try:
            last_conv_index = self.get_last_conv_id()
            logging.info('Last conv id: {}'.format(last_conv_index))

            conv_query = self.esp_config['conv_sync_query'].replace('{last_conv_id}', str(last_conv_index))
            convs_list = self.sql_client.list_query(conv_query, self.esp_config['conv_sync_query_keys'])
            logging.info('Got {}  new convs from tracking'.format(len(convs_list)))

            if len(convs_list) > 0:
                last_conv_index = convs_list[-1]['id'] + 1
                logging.info('Update Last conv id to {}'.format(last_conv_index))
                self.update_last_conv_id(last_conv_index)

            return convs_list
        except Exception as e:
            logging.error('Error getting new conversions.')
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return list()

    def get_last_conv_id(self):
        self.data_sql_client.commit()
        self.data_sql_client.command(self.esp_config['conv_last_id_querry'])
        result = self.data_sql_client.get_result()
        return result[0]

    def update_last_conv_id(self,index):
        self.data_sql_client.key_value_update('data.db_parameters',{'NAME':'email_dist_last_id','NUM_VALUE':index,'DATE_VALUE':datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")},'NAME')
        self.data_sql_client.commit()

    def insert_conv_record(self, conv_data):
        """ Returns a new conv object ready for DB insert. """
        conv_obj = dict()

        for conv_key in self.esp_config['conv_insert_keys_map'].keys():
            conv_obj[conv_key] = conv_data[self.esp_config['conv_insert_keys_map'][conv_key]]

        conv_obj['conv_type'] = self.calc_conv_type(conv_data)

        if conv_obj['conv_type'] != 'Optin' and conv_obj['conv_type'] != 'DUP_Optin':
            conv_obj['name'] = ''
        elif len(conv_obj['name']) <= 2:
            conv_obj['name'] = conv_data['name2']

        if conv_obj['product_id'] is None or conv_obj['product_id'] == 'None':
            conv_obj['product_id'] = 0

        conv_obj['paramvar_1'] = conv_data['aff_sub3']
        conv_obj['paramvar_4'] = conv_data['adv_sub3']

        if conv_obj['traffic_source'] == 'pushtrack':
            conv_obj['paramvar_2'] = conv_data['amount']
            conv_obj['paramvar_3'] = conv_data['event']

        self.sql_client.key_value_insert(self.esp_config['conv_table'], conv_obj)


    def calc_conv_type(self, conv_data):
        """ Returns a new conv object ready for DB insert. """
        if conv_data['ftd'] == 1:
            return 'FTD'
        if conv_data['dup_ftd'] == 1:
            return 'DUP_FTD'
        if conv_data['ftc'] == 1:
            return 'Lead'
        if conv_data['dup_ftc'] == 1:
            return 'DUP_Lead'
        if conv_data['fto'] == 1:
            return 'Optin'
        if conv_data['dup_fto'] == 1:
            return 'DUP_Optin'
        if conv_data['fte'] == 1:
            return 'Event'
        if conv_data['dup_fte'] == 1:
            return 'DUP_Event'

        return 'Unknown'
        

    def get_conv_dist_params(self, conv, delivery_types):
        """ Returns the parameters for the distribution of the conversion. """
        try:
            conv_dist_params_query = self.esp_config['get_conv_conf_query']
            for key in conv.keys():
                if key not in ('offer_id', 'aff_id', 'country_code', 'product_id'):
                    conv_dist_params_query = conv_dist_params_query.replace('{' + str(key) + '}', '\'' + str(conv[key]) + '\'')
                else:
                    conv_dist_params_query = conv_dist_params_query.replace('{' + str(key) + '}', str(conv[key]))

            conv_dist_params_query = conv_dist_params_query.replace('{delivery_types}', str(', '.join(f'"{dt}"' for dt in delivery_types)))
            conv_dist_params = self.sql_client.list_query(conv_dist_params_query, self.esp_config['get_conv_conf_query_keys'])
        except Exception as e:
            conv_dist_params = list()
            logging.error('Error getting dist params for conversion:' + str(conv))
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))

        return conv_dist_params


    def queue_new_conv(self, new_conv, dist_params):
        """ Queues the conversion and its parameters for distribution. """
        try:
            queue_query = self.esp_config['queue_conv_query']
            queue_query = queue_query.replace('{transaction_id}', new_conv['transaction_id'])
            queue_query = queue_query.replace('{time_to_send}', str(int(new_conv['conv_time']) + int(dist_params['delay'])*3600))
            queue_query = queue_query.replace('{status}', 'pending')
            queue_query = queue_query.replace('{list_id}', str(dist_params['list_id']))
            queue_query = queue_query.replace('{conversion_id}', str(new_conv['conversion_id']))
            queue_query = queue_query.replace('{net_list_id}', dist_params['net_list_id'])

            self.sql_client.command(queue_query)
            
            return 1
        except Exception as e:
            logging.error('Error replace parameters in query [queue_conv_query] for transaction_id' + new_conv['transaction_id'])
            return 0

    def mark_queued_convs(self, new_convs_list):
        """ Marks the new conversions as queued. """
        if len(new_convs_list) > 0:
            transaction_ids = ''

            for new_conv in new_convs_list:
                try:
                    transaction_ids += '\'' + str(new_conv['transaction_id']) + '\','
                except:
                    logging.error('Error marking queue conversion:' + str(new_conv))

            transaction_ids = transaction_ids[:-1]

            mark_queued_conversions_query = self.esp_config['mark_queued_conversions_query']
            mark_queued_conversions_query = mark_queued_conversions_query.replace('{transaction_ids}', str(transaction_ids))
            self.sql_client.command(mark_queued_conversions_query)

            self.sql_client.commit()

    def mark_queued_convs_by_conversion_id(self, new_convs_list):
        """ Marks the new conversions as queued. """
        if len(new_convs_list) > 0:
            conversion_ids = ''

            for new_conv in new_convs_list:
                try:
                    conversion_ids += '\'' + str(new_conv['conversion_id']) + '\','
                except:
                    logging.error('Error marking queue conversion:' + str(new_conv))

            conversion_ids = conversion_ids[:-1]

            mark_queued_conversions_query = self.esp_config['mark_queued_conversions_query_by_id']
            mark_queued_conversions_query = mark_queued_conversions_query.replace('{conversion_ids}', str(conversion_ids))
            self.sql_client.command(mark_queued_conversions_query)

            self.sql_client.commit()

    def klean13_email_check(self, conv):
        """ klean 13 email check. """
        if 'email' not in conv.keys() or conv['email'].strip() == '':
            logging.warning("conversion {conv_id} has no email".format(conv_id=conv['id']))
            return False

        headers = dict()
        headers['x-access-token'] = self.esp_config['klean13_x-access-code']
        headers['Content-Type'] = 'application/json'

        payload = "{\r\n    \"email\": \"" + str(conv['email']) + "\",\r\n    \"affiliate_id\":" + str(conv['aff_id']) + ",\r\n    \"offer_id\": " + str(conv['offer_id']) + ",\r\n    \"conversion_type\": " + str(conv['goal_id']) + "\r\n}"
        
        try:
            response = requests.request("POST", url=self.esp_config['klean13_url'], headers=headers, data=payload)
        except Exception as e:
            logging.error(str(e))
            logging.info("Error on send api-responce to klean13 for email: " + str(conv['email']) + " with error: " + str(e))
            return False

        res = json.loads(response.content)
        return res['status'] == True

    def everify_phone_check(self, conv):

        answer_res = 1
        if conv['phone'] == '':
            return False
        if len(conv['phone']) < 7:
            return False

        headers = dict()
        headers['x-access-token'] = self.esp_config['everify_phone_token']
        headers['Content-Type'] = 'application/json'

        payload = "{\"phone\": \"+" + str(conv['phone']) + "\"}"

        response = requests.request("POST", url=self.esp_config['everify_phone_url'], headers=headers, data=payload)
        res = json.loads(response.content)
        if 'is_valid' in res.keys():
            answer_res = res["is_valid"]
            return answer_res
        else:
            if response.status_code > 201:
                answer_res = True
                mess = ""
                if 'message' in res.keys():
                    mess = res["message"]
                logging.error("Error. Problem with phone verify service with message: " + str(mess) + ". Phone will be sending without verify")    
            else:
                answer_res = False    

        return answer_res #res["is_valid"]
