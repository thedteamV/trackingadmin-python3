""" ESPDist.py """
import sys
import traceback
import time
import json
import logging
import random
import requests
import traceback
from Service.Service import Service
from DB.Sql import Sql
from ESP.PushTracker import PushTracker


class Push77Dist(Service):
    """ Push77 Distribution class """

    def __init__(self, name='ESPDist', config_file=None, esp_config_file=None):
        """ Constructor. """
        super().__init__( name, self.worker, ["tracking_db"])

        self.sql_client = self.connection_hub.get("tracking_db")
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.config = None
        self.esp_config = None

        if self.config_file is None:
            logging.error('Push77Dist can not find config file: %s', self.name)
            exit()

        if self.esp_config_file is None:
            logging.error('Push77Dist can not find ESP config file: %s', self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as json_file:
                self.config = json.load(json_file)
                json_file.close()

            with open(self.esp_config_file, 'rb') as esp_json_file:
                self.esp_config = json.load(esp_json_file)
                esp_json_file.close()

            self.interval = self.get_interval('push77_dist_interval')
        except Exception as e:
            logging.error('Push77Dist could not connect to sql: %s', self.name)
            logging.error(e)

        logging.info('Push77Dist module is up: %s', self.name)


    def worker(self):
        """ Synchronized worker. """
        logging.info('Starting Push77Dist worker: %s', self.name)

        while self.active:
            try:
                self.worker_func()
            except Exception as e:
                logging.error('Error running worker function.' + str(e))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))

            time.sleep(self.interval)
        

    def worker_func(self):
        """ Synchronized worker functionality. """        
        self.preper_new_conversions()
        self.send_conversions()


    def preper_new_conversions(self):
        """ Prepers new conversions for sending. """
        convs_inserted_num = 0
        convs_queued_num = 0
        #Get last id for new conversions
        try:
            last_id_query = self.esp_config['pushtrack_get_last_id']
            self.sql_client.command(last_id_query)
            last_id = self.sql_client.get_result()[0]
        except:
            logging.error('Error inserting conversion:')
            logging.error("Select last id error: " + last_id_query)
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return

        # Gets new conversions from the tracking and enters it into the DB.
        new_convs_list = self.get_new_conversions(last_id)
        self.sql_client.commit()

        self.push77_manager = PushTracker('Pushtracker', self.config_file, self.esp_config_file, 0)
        self.push77_manager.connect()

        for conv in new_convs_list:
            try:
                self.insert_conv_record(conv)
                convs_inserted_num += 1
            except:
                logging.error('Error inserting conversion:')
                logging.error(str(conv))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
        self.sql_client.commit()

        # Gets the new conversions for queuing.
        new_convs_list = self.esp_config['new_push_convs_query']
        new_convs_list = self.sql_client.list_query(new_convs_list, self.esp_config['new_push_convs_query_keys'])

        # Queues the conversions.
        for new_conv in new_convs_list:
            try:
                convs_queued_num += self.queue_new_conv(new_conv)
            except:
                logging.error('Error queuing conversion:')
                logging.error('Conversion:' + str(new_conv))

        self.sql_client.commit()       

        # Marks the queued conversions in the conversions table.
        #self.mark_queued_convs(new_convs_list)
        self.mark_queued_convs_by_conversion_id(new_convs_list)

        # Prints log for the entire flow.
        logging.info('New conversions collected: {}'.format(len(new_convs_list)) + \
                     '  New conversions inserted: {}'.format(convs_inserted_num) + \
                     '  New conversions collected: ' + str(len(new_convs_list)) + \
                     '  New conversions queued: ' + str(convs_queued_num))


    def send_conversions(self):
        """ Sends queued conversions to the networks. """
        # Collects the pending conversions.
        pending_convs_query = self.esp_config['pending_push_convs_query']
        pending_convs_query = pending_convs_query.replace('{time}', str(int(time.time())))
        pending_convs_query = pending_convs_query.replace('{conv_types}', '\'Optin\',\'Lead\',\'Deposit\',\'FTD\',\'Event\'')
        convs_list = self.sql_client.list_query(pending_convs_query, self.esp_config['pending_push_convs_query_fields'])
        logging.info('Pending conversions found: %s', str(len(convs_list)))

        # Sends the conversions to the networks.
        self.add_contacts(convs_list)

        # Dequeues the sent conversions.
        try:
            if len(convs_list) > 0:
                queue_ids = ''
                for conv in convs_list:
                    queue_ids += '\'' + str(conv['queue_id']) + '\','
                queue_ids = queue_ids[:-1]

                dequeue_convs_query = self.esp_config['dequeue_sent_push_convs_query']
                dequeue_convs_query = dequeue_convs_query.replace('{time_sent}', str(int(time.time())))
                dequeue_convs_query = dequeue_convs_query.replace('{queue_ids}', queue_ids)
                self.sql_client.command(dequeue_convs_query)
                self.sql_client.commit()
        except:
            logging.info('Error dequeueing sent conversions')

        push_update_last_id = self.esp_config['pushtrack_update_last_id']
        self.sql_client.command(push_update_last_id)
        self.sql_client.commit()

    def get_new_conversions(self, last_conv_id):
        """ Returns a list of the new conversions from the DB. """
        try:
            conv_query = self.esp_config['conv_sync_push77_query'].replace('{last_conv_id}', str(last_conv_id))
            convs_list = self.sql_client.list_query(conv_query, self.esp_config['conv_sync_push77_query_keys'])

            return convs_list            
        except Exception as e:
            logging.error('Error getting new conversions.' + str(e))
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return list()


    def insert_conv_record(self, conv_data):
        """ Returns a new conv object ready for DB insert. """
        conv_obj = dict()

        for conv_key in self.esp_config['push_conv_insert_keys_map'].keys():
            if conv_key == 'status': 
                conv_obj[conv_key] = 'New'    
            else:    
                conv_obj[conv_key] = conv_data[self.esp_config['push_conv_insert_keys_map'][conv_key]]

        conv_obj['conv_type'] = self.calc_conv_type(conv_data)

        if conv_obj['product_id'] is None or conv_obj['product_id'] == 'None':
            conv_obj['product_id'] = 0
 
        self.sql_client.key_value_insert(self.esp_config['push_conv_table'], conv_obj)


    def calc_conv_type(self, conv_data):
        """ Returns a new conv object ready for DB insert. """
        if conv_data['ftd'] == 1:
            return 'FTD'
        if conv_data['dup_ftd'] == 1:
            return 'DUP_FTD'
        if conv_data['ftc'] == 1:
            return 'Lead'
        if conv_data['dup_ftc'] == 1:
            return 'DUP_Lead'
        if conv_data['fto'] == 1:
            return 'Optin'
        if conv_data['dup_fto'] == 1:
            return 'DUP_Optin'
        if conv_data['fte'] == 1:
            return 'Event'
        if conv_data['dup_fte'] == 1:
            return 'DUP_Event'

        return 'Unknown'
        
    def queue_new_conv(self, new_conv):
        """ Queues the conversion and its parameters for distribution. """
        try:
            queue_query = self.esp_config['queue_push_conv_query']
            queue_query = queue_query.replace('{transaction_id}', new_conv['transaction_id'])
            queue_query = queue_query.replace('{time_to_send}', str(int(new_conv['conv_time'])))
            queue_query = queue_query.replace('{status}', 'pending')
            #queue_query = queue_query.replace('{list_id}', str(dist_params['list_id']))
            queue_query = queue_query.replace('{conversion_id}', str(new_conv['conversion_id']))

            self.sql_client.command(queue_query)
            
            return 1
        except:
            return 0


    def mark_queued_convs(self, new_convs_list):
        """ Marks the new conversions as queued. """
        if len(new_convs_list) > 0:
            transaction_ids = ''

            for new_conv in new_convs_list:
                try:
                    transaction_ids += '\'' + str(new_conv['transaction_id']) + '\','
                except:
                    logging.error('Error marking queue conversion:' + str(new_conv))

            transaction_ids = transaction_ids[:-1]

            mark_queued_conversions_query = self.esp_config['mark_queued_conversions_query']
            mark_queued_conversions_query = mark_queued_conversions_query.replace('{transaction_ids}', str(transaction_ids))
            self.sql_client.command(mark_queued_conversions_query)

            self.sql_client.commit()

    def mark_queued_convs_by_conversion_id(self, new_convs_list):
        """ Marks the new conversions as queued. """
        if len(new_convs_list) > 0:
            conversion_ids = ''

            for new_conv in new_convs_list:
                try:
                    conversion_ids += '\'' + str(new_conv['conversion_id']) + '\','
                except:
                    logging.error('Error marking queue conversion:' + str(new_conv))

            conversion_ids = conversion_ids[:-1]

            mark_queued_conversions_query = self.esp_config['mark_queued_push_conversions_query_by_id']
            mark_queued_conversions_query = mark_queued_conversions_query.replace('{conversion_ids}', str(conversion_ids))
            self.sql_client.command(mark_queued_conversions_query)

            self.sql_client.commit()

    def add_contacts(self, contacts_list):
        """ Add contacts list to the networks. """
        contact_lists = dict()

        for user in contacts_list:
            if str(user['conversion_id']) not in contact_lists.keys():
                contact_lists[str(user['conversion_id'])] = list()

            contact = dict()
            contact['first_name'] = user['user_name']
            contact['last_name'] = ''
            contact['email'] = user['email']
            contact['country_code'] = user['country_code']
            contact['conversion_id'] = user['conversion_id']
            contact['conv_type'] = user['conv_type']

            try:
                contact['product_id'] = user['product_id']
            except Exception as e:
                logging.error('No product ID. Conversions ID: ' + str(contact['conversion_id']))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                contact['product_id'] = 0
            
            contact['message_id'] = user['paramvar_1']
            contact['amount'] = user['paramvar_2']
            contact['event'] = user['paramvar_3']
            
            contact_lists[str(user['conversion_id'])].append(contact)

        for list_id in contact_lists.keys():
            try:
                self.push77_manager.add_contacts(contact_lists[list_id])
            except Exception as e:
                logging.error('Error adding contacts to push77: ' + str(list_id))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                continue
        