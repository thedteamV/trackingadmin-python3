""" IndirectSync.py """
import time
import json
import logging
#import datetime
import requests
from Service.Service import Service
from urllib.parse import urlparse
from datetime import datetime,timedelta,timezone
from Utils.HelperFunct import HelperFunct


class IndirectSync(Service):
    """ Pixel sync class """

    def __init__(self, name='IndirectSync', config_file=None, local_config_file=None):
        """ Constructor. """
        super().__init__(name, self.worker, ["tracking_db", "data_db"])

        self.sql_client = self.connection_hub.get("tracking_db")
        self.sql_data_client = self.connection_hub.get("data_db")
        self.config_file = config_file
        self.auth_settings = {}
        self.config = None

        if self.config_file is None:
            logging.error('IndirectSync can not find config file: %s', self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as json_file:
                self.config = json.load(json_file)
                json_file.close()

            self.interval = self.get_interval('pixel_sync_interval')
            logging.info('IndirectSync is connected to sql: %s', self.name)
        except:
            logging.info('PixeIndirectSynclSync could not connect to sql: %s', self.name)

        try:
            with open('Config/local_config.json', 'rb') as local_json_file:
                self.auth_settings = json.load(local_json_file)['pixel_sync_auth']
                local_json_file.close()
        except Exception as e:
            logging.error('Error IndirectSync worker:' + str(e))
            logging.info('IndirectSync could not load local config file: %s', self.name)

        helper_funct = HelperFunct()
        self.trk_sys_arr = helper_funct.setTrksysidArray_byTrkSysId(self.sql_client, self.config)
        self.status_send, str_val = helper_funct.getParamFrom_db_parameters(self.sql_client, self.config, self.config['indi_status_to_send_param'])

        logging.info('IndirectSync module is up: %s', self.name)

    def worker(self):
        """ Synchronized worker. """
        logging.info('Starting synchronized worker: %s', self.name)
        logging.info('active state: %s', self.active)
        while self.active:
            try:
                if self.status_send == 1:
                    self.worker_func()
                else:
                    logging.info('IndirectSync is switched OFF in db_parameters')    

                time.sleep(self.interval)
            except Exception as e:
                logging.error('Error IndirectSync worker:' + str(e))
                time.sleep(self.interval)
                continue

    def worker_func(self):
        """" Synchronized worker functionality. """
        # Get the conversions data.
        logging.info('IndirectSync getting transactions to send from indirect_postback_log ...')
        try:
            indi_conversions = self._get_indi_conversions()
        except Exception as e:
            logging.exception('failed to get transactions to send from indirect_postback_log with: ' + str(e) )

        # Get the offer pixel urls.
        self.sql_client.commit()
        logging.info('got {num_convs} transactions'.format(num_convs=len(indi_conversions)))

        # sending
        headers_dict = dict()
        headers_dict['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        headers_dict['Content-Type'] = 'application/json'
        headers = headers_dict
        for indi_send in indi_conversions:
            try:
                status_sent = 0
                answer_code = 0
                answer_text = ""
                indi_data = dict()
                indi_data['id'] = indi_send['id']
                indi_data['et_id'] = indi_send['transaction_id']
                indi_data['st_id'] = indi_send['src_transaction_id']
                indi_data['channel'] = indi_send['channel']
                if indi_send['exp_date'] != None:
                    indi_data['exp_date'] = indi_send['exp_date'].isoformat()
                else:
                    indi_data['exp_date'] = ''    
                indi_data['conv_type'] = indi_send['orig_conv_type']

                body = json.dumps(indi_data)

                response_data = requests.post(indi_send['indirect_url'], headers=headers, data=body, timeout=5)

                if response_data.status_code == 200 or response_data.status_code == 201:
                    status_sent = 10
                    answer_code = str(response_data.status_code)
                    if response_data.text is not None:
                        answer_text = str(response_data.text)
                    logging.info ('Indirect data sent for id: ' + str(indi_data['id']) + '; body: ' + body)
                else:
                    status_sent = 2
                    answer_code = str(response_data.status_code)
                    logging.warn('worker_func: Error response from target id: ' + str(indi_data['id']) + '; body: ' + body)

                comm = self.config['sql_update_indi_transaction_to_done']
                comm = comm.format(status_sent, datetime.now(), answer_code, indi_send['indirect_url'], answer_text, indi_data['id'])

                self.sql_client.command(comm)
                self.sql_client.commit()


            except Exception as e:
                logging.error('worker_func: Error sending indirect data: id={}'.format(indi_data['id']) + str(e))
                continue

        logging.info ('Indirect data bulk sending finished. Last id: ' + str(indi_data['id']) )


    def _get_indi_conversions(self):

        results = []
        try:
            comm = self.config['sql_select_indi_transactions_to_send']
            comm = comm.format(datetime.now())
            self.sql_client.command(comm)
            conv_rec = self.sql_client.get_result()
            while conv_rec is not None:
                index = 0
                conv = {}

                for key in self.config['sql_select_indi_transactions_to_send_params']:
                    conv[key] = conv_rec[index]
                    index = index + 1

                results.append(conv)
                conv_rec = self.sql_client.get_result()
        except Exception as e:
            logging.error('_get_indi_conversions: Error in select and format data to send with: ' + str(e))
        
        return results


        

