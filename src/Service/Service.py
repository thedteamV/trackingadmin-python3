""" Service.py """
import logging
import threading
import json
from queue import Queue
from DB.ConnectionHub import ConnectionHub

class Service(object) :
    """ Service class """

    def __init__(self, name='Service', func=None, conn_name_list=None):
        """ Constructor. """
        self.name = name
        self.func = func
        self.thread = None
        self.active = False

        self.in_queue = dict()
        self.out_queue = dict()
        self.in_lock = dict()
        self.out_lock = dict()

        self.create_queues(self.name)

        self.connection_hub = ConnectionHub(config_file='Config/local_config.json')
        self.connection_hub.init(conn_name_list)

    def create_queues(self, queue_name):
        """ Creates set of queue and locks. """
        self.in_queue[queue_name] = Queue()
        self.out_queue[queue_name] = Queue()
        self.in_lock[queue_name] = threading.RLock()
        self.out_lock[queue_name] = threading.RLock()

    def get_queues(self, queue_name):
        """ Returns the queues. """
        return (self.in_queue[queue_name], self.out_queue[queue_name])

    def set_queues(self, queue_name, in_queue, out_queue):
        """ Sets the input and output queues. """
        self.in_queue[queue_name] = in_queue
        self.out_queue[queue_name] = out_queue

    def get_locks(self, queue_name):
        """ Returns the locks. """
        return (self.in_lock[queue_name], self.out_lock[queue_name])

    def set_locks(self, queue_name, in_lock, out_lock):
        """ Sets the input and output queues. """
        self.in_lock[queue_name] = in_lock
        self.out_lock[queue_name] = out_lock

    def get_interval(self, name):
        with open('Config/local_config.json', 'rb') as data_json_file:
           interval_config = json.load(data_json_file)['threads']['intervals']
           data_json_file.close()

        return interval_config[name]

    def get_server(self):
        with open('Config/local_config.json', 'rb') as data_json_file:
           server = json.load(data_json_file)['server']
           data_json_file.close()

        return server

    def start(self):
        """ Starts the service. """
        if self.func is None:
            return False

        logging.info('Starting thread: %s', self.name)

        self.active = True
        self.thread = threading.Thread(target=self.func, name=self.name)
        self.thread.start()

        return True

    def stop(self):
        """ Stops the service. """
        if self.thread is None:
            return False

        logging.info('Stopping thread: %s', self.name())
        self.active = False
        self.thread.join()
        logging.info('%s thread has been closed', self.name())

        self.thread.exit()

        return True
