""" PixelSync.py """
import time
import json
import logging
#import datetime
import requests
from Service.Service import Service
from urllib.parse import urlparse
from datetime import datetime,timedelta,timezone
from Utils.HelperFunct import HelperFunct


class PixelSync(Service):
    """ Pixel sync class """

    def __init__(self, name='PixelSync', config_file=None, local_config_file=None):
        """ Constructor. """
        super().__init__(name, self.worker, ["tracking_db", "data_db"])

        self.sql_client = self.connection_hub.get("tracking_db")
        self.sql_data_client = self.connection_hub.get("data_db")
        self.config_file = config_file
        self.auth_settings = {}
        self.config = None

        if self.config_file is None:
            logging.error('PixelSync can not find config file: %s', self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as json_file:
                self.config = json.load(json_file)
                json_file.close()

            self.interval = self.get_interval('pixel_sync_interval')
            logging.info('PixelSync is connected to sql: %s', self.name)
        except:
            logging.info('PixelSync could not connect to sql: %s', self.name)

        try:
            with open('Config/local_config.json', 'rb') as local_json_file:
                self.auth_settings = json.load(local_json_file)['pixel_sync_auth']
                local_json_file.close()
        except Exception as e:
            logging.error('Error PixelSync worker:' + str(e))
            logging.info('PixelSync could not load local config file: %s', self.name)

        helper_funct = HelperFunct()
        self.trk_sys_arr = helper_funct.setTrksysidArray_byTrkSysId(self.sql_client, self.config)

        logging.info('PixelSync module is up: %s', self.name)

    def worker(self):
        """ Synchronized worker. """
        logging.info('Starting synchronized worker: %s', self.name)
        logging.info('active state: %s', self.active)
        while self.active:
            try:
                self.worker_func()
                self.sql_data_client.key_value_update(
                    'data.db_parameters',
                    {'NAME': 'err_alert_settings_pixel_sync', 'NUM_VALUE': 0,
                     'DATE_VALUE': datetime.now(timezone.utc).strftime("%y-%m-%d %H:%M:%S")},
                    "NAME"
                )
                time.sleep(self.interval)
            except Exception as e:
                logging.error('Error PixelSync worker:' + str(e))
                time.sleep(self.interval)
                continue

    def worker_func(self):
        """" Synchronized worker functionality. """
        # Get the conversions data.
        logging.info('PixelSync getting conversion...')
        try:
            conversions = self._get_conversions()
            country_code_to_amount = self._get_country_code_to_amount()
        except Exception as e:
            logging.exception('failed to get conversions')

        # Get the offer pixel urls.
        pixels = []
        self.sql_client.commit()
        logging.info('got {num_convs} conversions'.format(num_convs=len(conversions)))
        for conv in conversions:
            try:
                comm = self.config['pixel_url_query']
                comm = comm.format(conv['affiliate_id'], conv['goal_id'], conv['offer_id'], conv['trk_sys_id'])
                self.sql_client.command(comm)
                try:
                    pixels_data = self.sql_client.get_results()
                except Exception as e:
                    continue

                conv['offer_id_org'] = int(conv['offer_id'])
                if 'trk_sys_id' in conv.keys() and int(conv['trk_sys_id']) > 0:
                    offer_src = int(conv['offer_id']) - int(self.trk_sys_arr[conv['trk_sys_id']]['inc_offers'])
                    aff_src = int(conv['affiliate_id']) - int(self.trk_sys_arr[conv['trk_sys_id']]['inc_affiliates'])

                    #conv['offer_id'] = int(conv['offer_id']) - 10000 * int(conv['trk_sys_id'])
                    #conv['affiliate_id_norm'] = int(conv['affiliate_id']) - 10000 * int(conv['trk_sys_id'])
                    conv['offer_id'] = offer_src
                    conv['affiliate_id_norm'] = aff_src
                else:
                    conv['affiliate_id_norm'] = conv['affiliate_id']

                if int(conv['ftd']) == 1:
                    conv['conv_type'] = 'deposit'
                elif int(conv['ftc']) == 1:
                    conv['conv_type'] = 'lead'
                elif int(conv['fto']) == 1:
                    conv['conv_type'] = 'optin'
                elif int(conv['fte']) == 1 and conv['event'] == 'PA':
                    conv['conv_type'] = 'pa'
                else:
                    conv['conv_type'] = ''

                for pixel_data in pixels_data:
                    pixel_id, pixel_url, price_per_country = pixel_data
                    try:
                        pixel_price_type = "no"
                        if conv['conv_type'] == 'deposit':
                            pixel_price_type = "regular"
                            if self._is_invalid_amount(conv):
                                 continue
                            elif self.config['is_price_country_affected']:
                                if pixel_url is not None:
                                    country_code = conv['country_code']
                                    pixel_price_per_country = self._get_pixel_country_amount(pixel_id, country_code)
                                    if pixel_price_per_country:
                                        amount = pixel_price_per_country
                                        pixel_price_type = "pixel_price_per_country"
                                    elif price_per_country:
                                        amount = country_code_to_amount.get(country_code, 0)
                                        pixel_price_type = "price_per_country"
                                    else:
                                        amount = conv['amount']
                            else:
                                amount = conv['amount']
                                    #     logging.warning("no price per country, pixel id {pixel_id}".format(pixel_id=pixel_id))
                                    #     #raise Exception("no price per country, pixel id {pixel_id}".format(pixel_id=pixel_id))
                            conv['amount'] = amount
                    except Exception as e:
                        continue

                for pixel_data in pixels_data:
                    pixel_id, pixel_url, price_per_country = pixel_data
                    try:
                        pixel_url = str(pixel_url)
                        try:
                            if float(conv['amount']) == 0.0:
                                conv['amount'] = ''
                        except:
                            conv['amount'] = ''

                        for key in conv.keys():
                            pixel_url = pixel_url.replace('{' + key + '}', str(conv[key]))

                        pix = {}
                        pix['pixel_time'] = int(time.time())
                        pix['conv_id'] = conv['conv_id']
                        pix['click_id'] = conv['click_id']
                        pix['transaction_id'] = conv['transaction_id']
                        pix['pixel_url'] = pixel_url
                        #pix['pixel_price_type'] = pixel_price_type
                        pix['pixel_id'] = pixel_id
                        pix['aff_id'] = conv['affiliate_id']
                        pix['offer_id'] = conv['offer_id_org']
                        pix['goal_id'] = conv['goal_id']
                        pix['trk_sys_id'] = conv['trk_sys_id']
                        if conv['conv_type_id'] is not None:
                            pix['conv_type_id'] = conv['conv_type_id']
                        else:
                            pix['conv_type_id'] = 0
                        pix['pushuserid'] = ''
                        pix['to_message'] = ''
                        pix['status_sent'] = 0
                        pixels.append(pix)
                    except Exception as e:
                        continue

            except:
                self.sql_client.clear_results()
                continue

        find_stid = self.config['postback_srctransaction_param_name']
        for pix in pixels:
            payout = self.get_payout(conv)
            to_message = ''
            src_pushuserid = '0'
            pix['pixel_url'] = pix['pixel_url'].replace('{payout}', str(payout))
            pix['pixel_url'] = self._apply_auth_params(pix['pixel_url'])
            aa = pix['pixel_url'].find(find_stid) 
            if pix['pixel_url'].find(find_stid) >= 0:
                try:
                    st_id, to_message, src_pushuserid = self.setSourceTransactionid(conv['transaction_id'], pix['click_id'])
                    pix['pixel_url'] = pix['pixel_url'].replace('{source_trid}', str(st_id))
                except Exception as e:
                    logging.error('tr: ' + pix['transaction_id'] + ': exception in searching source transaction_id for PA event conv_id=' + str(pix['conv_id']) + '; pixel_id=' + str(pix['pixel_id']))        
                    continue
            pix['to_message'] = to_message
            pix['pushuserid'] = src_pushuserid

        # Fire and log the pixels.
        for pix in pixels:
            try:
                # fire the pixel!!!
                headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
                response = requests.get(pix['pixel_url'], headers=headers, timeout=5)

                if response.status_code != 200:
                    logging.warn('Error response from target: tid={}'.format(pix['transaction_id']) + ' ' + pix['to_message'])
                    comm = self.config['pixel_error_inc'].format(pix['conv_id'])
                    self.sql_client.command(comm)
                    self.sql_client.commit()
                else:
                    logging.info('Pixel sent: pixel_id={}, aff_id={}, conv_type={}, tid={}, url={}'.format(str(pix['pixel_id']),str(pix['aff_id']),str(pix['conv_type_id']), pix['transaction_id'], pix['pixel_url']) + ' ' + pix['to_message'])

                comm = self.config['pixel_sent_inc'].format(pix['conv_id'])
                self.sql_client.command(comm)
                self.sql_client.commit()

                pix_to_db = {}
                pix_to_db['pixel_time'] = pix['pixel_time']
                pix_to_db['conv_id'] = pix['conv_id']
                pix_to_db['transaction_id'] = pix['transaction_id']
                pix_to_db['pixel_url'] = pix['pixel_url']
                pix_to_db['pixel_id'] = pix['pixel_id'] 
                pix_to_db['aff_id'] = pix['aff_id'] 
                pix_to_db['offer_id'] = pix['offer_id'] 
                pix_to_db['goal_id'] = pix['goal_id'] 
                pix_to_db['trk_sys_id'] = pix['trk_sys_id']
                pix_to_db['conversion_type'] = pix['conv_type_id']
                pix_to_db['sent_time'] = datetime.now()
                pix_to_db['paramvar_1'] = pix['pushuserid']
                pix_to_db['status_sent'] = 10
                #self.sql_client.key_value_insert('pixels_log', pix)
                self.sql_client.key_value_insert('pixels_log', pix_to_db)
                self.sql_client.commit()

            except Exception as e:
                logging.error('Error sending pixel: tid={}'.format(pix['transaction_id']) + str(e))
                continue

    def _apply_auth_params(self, url):
        parsed = urlparse(url)
        hostname = parsed.hostname

        if hostname not in self.auth_settings:
            return url

        auth_settings = self.auth_settings.get(hostname)
        url = url.strip().strip('&').strip('?')
        return url + ('&' if urlparse(url).query else '?') + "access_account={}&access_token={}".format(auth_settings['access_account'], auth_settings['access_token'])

    def _get_country_code_to_amount(self):
        comm = self.config['country_data_query']
        self.sql_client.command(comm)
        country_code_to_amount = {x[1]: x[2] for x in self.sql_client.get_results()}
        return country_code_to_amount

    #TODO: ask marina, is this a one time thing? or should we lazy load all pixels to amount before?
    def _get_pixel_country_amount(self, pixel_id, country_code):
        comm = self.config['pixel_country_amount_query']
        comm = comm.format(pixel_id=pixel_id, country_code=country_code)
        self.sql_client.command(comm)
        pixel_country_amount = self.sql_client.get_result()
        if pixel_country_amount:
            return pixel_country_amount[0]
        return None

    def _get_conversions(self):
        affiliates = self.sql_client.list_query("SELECT affiliate_id, ftd, fto, ftc, fte_pa FROM whitelist_affiliates", ['affiliate_id',  'ftd', 'fto', 'ftc', 'fte_pa'])
        offers = self.sql_client.list_query("SELECT offer_id, ftd, fto, ftc, fte_pa FROM whitelist_offers", ['offer_id', 'ftd', 'fto', 'ftc', 'fte_pa'])
        trk_sys_ids = self.sql_client.list_query("SELECT trk_sys_id, ftd, fto, ftc, fte_pa FROM whitelist_trk_sys", ['trk_sys_id', 'ftd', 'fto', 'ftc', 'fte_pa'])

        # affiliates_fto_whitelist
        filtered = list(filter(lambda e: e['fto'] == 1, affiliates))
        affiliates_fto_whitelist = list(map(lambda e: e['affiliate_id'], filtered))

        # affiliates_ftc_whitelist
        filtered = list(filter(lambda e: e['ftc'] == 1, affiliates))
        affiliates_ftc_whitelist = list(map(lambda e: e['affiliate_id'], filtered))

        #affiliates_ftd_whitelist
        filtered = list(filter(lambda e: e['ftd'] == 1, affiliates))
        affiliates_ftd_whitelist = list(map(lambda e: e['affiliate_id'], filtered))

        #affiliates_fte_pa_whitelist
        filtered = list(filter(lambda e: e['fte_pa'] == 1, affiliates))
        affiliates_fte_pa_whitelist = list(map(lambda e: e['affiliate_id'], filtered))

        #offers_fto_whitelist
        filtered = list(filter(lambda e: e['fto'] == 1, offers))
        offers_fto_whitelist = list(map(lambda e: e['offer_id'], filtered))

        # offers_ftc_whitelist
        filtered = list(filter(lambda e: e['ftc'] == 1, offers))
        offers_ftc_whitelist = list(map(lambda e: e['offer_id'], filtered))

        #offers_ftd_whitelist
        filtered = list(filter(lambda e: e['ftd'] == 1, offers))
        offers_ftd_whitelist = list(map(lambda e: e['offer_id'], filtered))

        # offers_fte_pa_whitelist
        filtered = list(filter(lambda e: e['fte_pa'] == 1, offers))
        offers_fte_pa_whitelist = list(map(lambda e: e['offer_id'], filtered))

        #trk_sys_id_ftc_whitelist
        filtered = list(filter(lambda e: e['ftc'] == 1, trk_sys_ids))
        trk_sys_id_ftc_whitelist = list(map(lambda e: e['trk_sys_id'], filtered))

        #trk_sys_id_ftd_whitelist
        filtered = list(filter(lambda e: e['ftd'] == 1, trk_sys_ids))
        trk_sys_id_ftd_whitelist = list(map(lambda e: e['trk_sys_id'], filtered))

        # trk_sys_id_fto_whitelist
        filtered = list(filter(lambda e: e['fto'] == 1, trk_sys_ids))
        trk_sys_id_fto_whitelist = list(map(lambda e: e['trk_sys_id'], filtered))

        # trk_sys_id_fte_pa_whitelist
        filtered = list(filter(lambda e: e['fte_pa'] == 1, trk_sys_ids))
        trk_sys_id_fte_pa_whitelist = list(map(lambda e: e['trk_sys_id'], filtered))

        #comm = self.config['pixel_data_query']
        comm = self.config['pixel_data_query_extended']
        comm = comm.replace('{affiliates_fto_whitelist}', ','.join(str(x) for x in affiliates_fto_whitelist)) \
                   .replace('{offers_fto_whitelist}', ','.join(str(x) for x in offers_fto_whitelist)) \
                   .replace('{trk_sys_id_fto_whitelist}', ','.join(str(x) for x in trk_sys_id_fto_whitelist)) \
                   .replace('{affiliates_ftc_whitelist}', ','.join(str(x) for x in affiliates_ftc_whitelist)) \
                   .replace('{offers_ftc_whitelist}', ','.join(str(x) for x in offers_ftc_whitelist)) \
                   .replace('{trk_sys_id_ftc_whitelist}', ','.join(str(x) for x in trk_sys_id_ftc_whitelist)) \
                   .replace('{affiliates_ftd_whitelist}', ','.join(str(x) for x in affiliates_ftd_whitelist)) \
                   .replace('{offers_ftd_whitelist}', ','.join(str(x) for x in offers_ftd_whitelist)) \
                   .replace('{trk_sys_id_ftd_whitelist}', ','.join(str(x) for x in trk_sys_id_ftd_whitelist)) \
                   .replace('{affiliates_fte_pa_whitelist}', ','.join(str(x) for x in affiliates_fte_pa_whitelist)) \
                   .replace('{offers_fte_pa_whitelist}', ','.join(str(x) for x in offers_fte_pa_whitelist)) \
                   .replace('{trk_sys_id_fte_pa_whitelist}', ','.join(str(x) for x in trk_sys_id_fte_pa_whitelist)) \
                   .replace('{last_conv_time}', str(int(time.time() - 3600 * 24 * self.config['postback_max_days_old'])))
        self.sql_client.command(comm)
        conv_rec = self.sql_client.get_result()
        results = []
        while conv_rec is not None:
            index = 0
            conv = {}

            #for key in self.config['pixel_data_query_fields']:
            for key in self.config['pixel_data_query_fields_extended']:
                conv[key] = conv_rec[index]
                index = index + 1

            results.append(conv)
            conv_rec = self.sql_client.get_result()
        return results

    def get_payout(self, conv):
        """ Returns the payout for the conversion. """
        payout = self.get_offer_payout(conv)

        if payout is not None:
            return payout

        return self.get_default_payout(conv)

    def get_default_payout(self, conv):
        """ Returns the default payout for the conversion (level 3). """
        try:
            if conv['goal_id'] == 0 or conv['goal_id'] == '0':
                comm = self.config['default_deposit_payout_data_query']
                comm = comm.format(conv['offer_id'])
                self.sql_client.command(comm)

                payout = self.sql_client.get_result()[0]

                if payout is None:
                    return 0.01

                return payout

            else:
                comm = self.config['default_conv_payout_data_query']
                comm = comm.format(conv['offer_id'], conv['goal_id'])
                self.sql_client.command(comm)

                payout = self.sql_client.get_result()[0]

                if payout is None:
                    return 0.01

                return payout

        except:
            return 0.01

    def get_offer_payout(self, conv):
        """ Returns the payout for the conversion from offer_payout table (level 2). """
        try:
            comm = self.config['offer_payout_data_query']
            comm = comm.format(conv['affiliate_id'], conv['offer_id'], conv['goal_id'])
            self.sql_client.command(comm)
            payout_data = self.sql_client.get_result()

            if payout_data is None:
                return None

            payout = payout_data[0]
            payout_precent = payout_data[1]
            payout_type = payout_data[2]

            if payout_type != 'CPA' and (conv['goal_id'] == 0 or conv['goal_id'] == '0'):
                return 0.01
            return payout
        except:
            return None

    def _is_invalid_amount(self, conv):
        min_deposit_amount = self.config['min_deposit_amount']
        return conv['conv_type'] == 'deposit' and ('amount' not in conv.keys() or conv['amount'] =='' or
            conv['amount'] is None or (float(conv['amount']) < min_deposit_amount and conv['status'] != 'approved'))

    def setSourceTransactionid(self, transaction_id, click_id):
        """ set source transaction_id by click_id through pushuserid """

        to_message = ''
        src_pushuserid = '0'
        min_source_date = datetime.now() - timedelta(days=self.config['postback_srctransaction_days_ago'])
        min_source_time = time.mktime(min_source_date.timetuple())

        try:
            comm = self.config['pixel_find_source_transid_any_conversion']
            comm = comm.format(click_id)
            self.sql_client.command(comm)
            result = self.sql_client.get_result()
            if result is None:
                return '0', ' record not found in pa_collection for click_id: ' + str(click_id) + '', src_pushuserid
            src_transaction_id = result[0]
            src_click_time = result[1]
            src_pushuserid = result[2]
            if src_click_time > 0:
                if src_click_time > min_source_time:
                    to_message += ' 1.: source transaction_id found with click_time: ' + str(src_click_time)
                    return src_transaction_id, to_message, src_pushuserid
                else:
                    to_message += ' 2.: source transaction_id found with expire date: ' + str(src_click_time)
                    return '0', to_message, src_pushuserid
            else:
                to_message += ' 3.: source transaction_id not found for pushuserid'
        except Exception as e:
            #logging.error(str(e))
            to_message =  ' setSourceTransactionid: cannot set result for pixel_srctransaction_find_srctransaction query: ' + str(comm)
            return '0', to_message, src_pushuserid


        

