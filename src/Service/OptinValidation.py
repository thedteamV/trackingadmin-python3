""" OptinValidation.py """
import time
import json
import logging
import random
import datetime
from Service.Service import Service


class OptinValidation(Service):
    """ Optin validation class """

    def __init__(self, name='OptinValidation', config_file=None):
        """ Constructor. """
        super().__init__( name, self.worker, ["tracking_db", "data_db"])

        self.sql_client = self.connection_hub.get("tracking_db")
        self.sql_data_client = self.connection_hub.get("data_db")
        self.config_file = config_file
        self.config = None

        if self.config_file is None:
            logging.error('OptinValidation can not find config file: %s', self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as json_file:
                self.config = json.load(json_file)
                json_file.close()

            self.interval = self.get_interval('optin_check_interval')
            logging.info('OptinValidation is connected to sql: %s', self.name)
        except:
            logging.info('OptinValidation could not connect to sql: %s', self.name)

        logging.info('OptinValidation module is up: %s', self.name)


    def worker(self):
        """ Synchronized worker. """
        logging.info('Starting OptinValidation worker: %s', self.name)

        while self.active:
            try:
                self.worker_func()
                self.sql_data_client.key_value_update(
                    'data.db_parameters',
                    {'NAME': 'err_alert_settings_opt_valid', 'NUM_VALUE': 0,
                     'DATE_VALUE': datetime.datetime.now(datetime.timezone.utc).strftime("%y-%m-%d %H:%M:%S")},
                    "NAME"
                )
            except Exception as e:
                logging.error('Error running worker function.')
                logging.error(e)

            time.sleep(self.interval)
        

    def worker_func(self):
        """" Synchronized worker functionality. """
        # Get the optin data.
        approved = 0
        rejected = 0
        untracked = 0

        optins = list()
        self.sql_client.commit()

        comm = self.config['optin_check_get_list']
        self.sql_client.command(comm)

        optin_rec = self.sql_client.get_result()

        while optin_rec is not None:
            index = 0
            optin = dict()

            for key in self.config['optin_check_get_list_fields']:
                optin[key] = optin_rec[index]
                index = index + 1
            
            optins.append(optin)
            optin_rec = self.sql_client.get_result()


        aff_tracking = self.sql_client.dict_query(self.config['optin_aff_track_query'], self.config['optin_aff_track_query_fields'])
 
        # Get optin count and set the status.
        for optin in optins:
            try:
                comm = self.config['optin_verification']
                comm = comm.format(optin['email'])
                self.sql_client.command(comm)

                optin_count = int(self.sql_client.get_result()[0])
                self.check_optin_tracking(aff_tracking, optin['aff_id'])

                self.sql_client.clear_results()

                if optin_count > 0:
                    set_comm = self.config['optin_status_update'].format('rejected', optin['id'])
                    rejected += 1
                else:
                    if self.check_optin_tracking(aff_tracking, optin['aff_id']):
                        set_comm = self.config['optin_status_update'].format('approved', optin['id'])
                        approved += 1
                    else:
                        set_comm = self.config['optin_status_update'].format('untracked', optin['id'])
                        untracked += 1

                self.sql_client.command(set_comm)
                self.sql_client.clear_results()
                self.sql_client.commit()

            except:
                self.sql_client.clear_results()
                continue

        logging.info('Optins validation. Approved: {}, Untracked: {}, Rejected: {}'.format(approved, untracked, rejected))


    def check_optin_tracking(self, aff_tracking, aff_id):
        """ Check the untracked value for the optin. """
        try:
            if str(aff_id) not in aff_tracking.keys():
                return True

            track_value = int(aff_tracking[str(aff_id)]['tracking'])
            rand_track = random.randint(0, 100)

            if track_value > rand_track:
                return True
            else:
                return False
        except:
            return False
