import mysql.connector

class Sql(object):

    def __init__(self):
        self.mUser = 'root'
        self.mPassword = ''
        self.mHost = '127.0.0.1'
        self.mPort = '3306'
        self.mDatabase = 'admin'
        self.mCharset = 'utf8'
        self.mCollection = 'utf8_general_ci'
        self.mConnection = None
        self.mCursor = None

    # Sets the SQL parameters.
    def set_parameters(self, User='', Password='', Host='127.0.0.1', Port='3306', Database='admin'):
        self.mUser = User
        self.mPassword = Password
        self.mHost = Host
        self.mPort = Port
        self.mDatabase = Database
        self.mCharset = 'utf8'
        self.mCollection = 'utf8_general_ci'
        self.mConnection = None
        self.mCursor = None

    # Connects to the SQL server.
    def connect(self):
        try:
            self.mConnection = mysql.connector.connect(user=self.mUser, password=self.mPassword,
                                                       host=self.mHost, port=self.mPort,
                                                       database=self.mDatabase)
        except mysql.connector.Error as con_error:
            raise con_error

        self.mCursor = self.mConnection.cursor()

    # Executes any SQL command.
    def command(self, Command):
        if (self.mConnection is None) or (self.mCursor is None):
            return

        if not self.mConnection.is_connected():
            self.connect()

        self.mCursor.execute(Command)

    # Executes any SQL command and commits it.
    def command_commit(self, Command):
        if (self.mConnection is None) or (self.mCursor is None):
            return

        if not self.mConnection.is_connected():
            self.connect()

        self.mCursor.execute(Command)
        self.mConnection.commit()

    # Commits the left data.
    def commit(self):
        if self.mConnection is None:
            return

        if not self.mConnection.is_connected():
            self.connect()

        self.mConnection.commit()

    def get_result(self):
        return self.mCursor.fetchone()

    def get_results(self):
        return self.mCursor.fetchall()

    def get_results_dict(self):
        rows = self.mCursor.fetchall()
        headers = [i[0] for i in self.mCursor.description]
        dict_rows = []
        for row in rows:
            dict_row = {headers[i]: row_value for i,row_value in enumerate(row)}
            dict_rows.append(dict_row)
        return dict_rows

    def clear_results(self):
        temp = self.mCursor.fetchone()
        while not temp is None:
            temp = self.mCursor.fetchone()

    # Closes the connection.
    def close(self):
        if self.mConnection != None:
            self.mConnection.commit()
            self.mConnection.close()

        if self.mCursor != None:
            self.mCursor.close()

    # Inserts key value dictionary to the table as row.
    def key_value_insert(self, table, data):
        cols = '('
        values = 'VALUES('

        for key in data.keys():
            try:
                cols = cols + str(key) + ','
                values = values + '\'' + str(data[key]).replace('\'', '') + '\','
            except:
                values = ''

        cols = cols[:-1] + ')'
        values = values[:-1] + ')'

        comm = 'INSERT INTO ' + str(table) + ' ' +  cols + ' ' + values
        self.command(comm)


    def last_insert_id(self):
        return self.mCursor.lastrowid


    # Updates key value dictionary to the table as row.
    def key_value_update(self, table, data, id_key):
        sets = ''

        for key in data.keys():
            try:
                sets = sets + '`' + str(key) + '`' + ' = ' + '\'' + str(data[key]).replace('\'', '') + '\','
            except:
                sets = ''

        sets = sets[:-1]

        comm = 'UPDATE ' + str(table) + ' SET ' +  sets + ' WHERE ' + str(id_key) + '=\'' + str(data[id_key]) + '\''

        self.command_commit(comm)


    # Performs the query and returns the results in a dictionary.
    def dict_query(self, query, keys, main_key_index=0):
        results = dict()

        self.commit()
        self.command(query)

        row = self.get_result()

        while row is not None:
            results[str(row[main_key_index])] = dict()

            index = 0

            for key in keys:
                results[str(row[main_key_index])][str(key)] = row[index]
                index += 1

            row = self.get_result()

        self.clear_results()

        return results

    # Performs the query and returns the results in a list.
    def list_query(self, query, keys):
        results = list()

        self.commit()
        self.command(query)

        row = self.get_result()

        while row is not None:
            result = dict()

            index = 0

            for key in keys:
                result[str(key)] = row[index]
                index += 1

            results.append(result)

            row = self.get_result()

        self.clear_results()
        
        return results
