""" Mongo.py """
from pymongo import MongoClient
from bson import ObjectId


class Mongo(object):
    """ Class docstring """

    def __init__(self):
        self.user = None
        self.password = None
        self.host = '127.0.0.1'
        self.port = 27017
        self.db_name = None

        self.client = None
        self.db_client = None

    def set_params(self, user=None, password=None, host='127.0.0.1',
                   port=27017, db_name=None):
        """ Method docstring """
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.db_name = db_name

    def connect(self):
        """ Method docstring """
        self.client = MongoClient(self.host, self.port)

        if self.db_name is not None:
            self.set_db(self.db_name)

    def set_db(self, db_name):
        """ Method docstring """
        self.db_client = self.client[db_name]
        self.db_name = db_name
        self.db_client.authenticate(self.user, password=self.password)

    def insert_doc(self, col, doc):
        """ Method docstring """
        doc_id = self.db_client[col].insert(doc)
        return doc_id

    def insert(self, col, docs):
        """ Method docstring """
        docs_id = self.db_client[col].insert(docs)
        return docs_id

    def delete(self, col, cond=None):
        """ Method docstring """
        doc_id = self.db_client[col].remove(cond)
        return doc_id

    def find_doc(self, col, cond=None):
        """ Method docstring """
        return self.db_client[col].find_one(cond)

    def find_doc_by_number(self, col, doc_num=1, count=1):
        """ Method docstring """
        return self.db_client[col].find().skip(doc_num).limit(count)

    def find(self, col, cond=None):
        """ Method docstring """
        return self.db_client[col].find(cond)

    def count(self, col, cond=None):
        """ Method docstring """
        if cond is None:
            return self.db_client[col].count()

        return self.db_client[col].find(cond).count()

    def update_doc(self, col, doc, mongo_id):
        """ Method docstring """
        if not isinstance(mongo_id, ObjectId):
            mongo_id = ObjectId(mongo_id)

        doc['_id'] = mongo_id
        return self.db_client[col].update_one({'_id': mongo_id}, {'$set': doc})
