from DB.Sql import Sql
from redis import Redis
import sys
import traceback
import logging
import json
import os


class ConnectionHub(object):
    def __init__(self, config_file=None):
        self.config_file = config_file
        self.connections = dict()
        self.name = 'ConnectionHub'

    def create(self, config=None, name=None):
        try:
            if config['driver'] == 'mysql':
                client = Sql()
                client.set_parameters(
                    User=os.getenv(name+'_user'),
                    Password=os.getenv(name+'_pass'),
                    Host=os.getenv(name+'_host'),
                    Port=os.getenv(name+'_port'),
                    Database=os.getenv(name+'_db'))
                client.connect()
                logging.info('Connected to: %s', name)
                return client

            elif config['driver'] == 'redis':
                client = Redis(
                    host=config['host'],
                    password=config['pass']
                )
                logging.info('Connected to: %s', name)
                return client

        except Exception as e:
            logging.error('%s could not connect to %s: %s', self.name, config['driver'], name)
            logging.error(str(e))

    def get(self, name):
        return self.connections[name]

    def init(self, conn_names=list):
        if self.config_file is None:
            logging.error('%s can not find config file: %s', self.name, self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as data_json_file:
                conn_list = json.load(data_json_file)['connections']
                data_json_file.close()
            for name, conn_params in conn_list.items():
                if name in conn_names:
                    connection = self.create(conn_params, name)
                    self.connections[name] = connection
        except Exception as e:
            logging.error('%s could not read config file %s', self.name, self.config_file)
            logging.error(e)
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
