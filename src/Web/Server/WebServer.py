""" WebServer.py """
import logging
import tornado.ioloop
import tornado.web
import asyncio
from Service.Service import Service
from Web.Handlers.ConvHandler import ConvHandler
from Web.Handlers.BrandsPayoutHandler import BrandsPayoutHandler
from Web.Handlers.BrandsPayoutReportHandler import BrandsPayoutReportHandler
from Web.Handlers.CountriesPayoutHandler import CountriesPayoutHandler
from Web.Handlers.ESPDistHandler import ESPDistHandler
from Web.Handlers.GDPRHandler import GDPRHandler
from Web.Handlers.OffersHandler import OffersHandler
from Web.Handlers.PixelCountriesPayoutHandler import PixelCountriesPayoutHandler
from Web.Handlers.PixelListHandler import PixelListHandler
from Web.Handlers.PixelHandler import PixelHandler
from Web.Handlers.AuditHandler import AuditHandler
from Web.Handlers.WhitelistHandler import WhitelistHandler
from Web.Handlers.HealthCheckHandler import HealthCheckHandler


class WebServer(Service):
    """ Web server class. """
    def __init__(self, port=80):
        """ Constructor. """
        Service.__init__(self, name='WebServer', func=self.run_server, conn_name_list=['tracking_db', 'data_db'])
        self.port = port
        self.app = None

    def set_routes(self):
        """ Creates the application routes """
        return tornado.web.Application([
            (r"/api/v1/convs", ConvHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/brands_payout", BrandsPayoutHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/countries_payout", CountriesPayoutHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/pixel_countries_payout", PixelCountriesPayoutHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/reports/brands_payout", BrandsPayoutReportHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/gdpr/delete_user", GDPRHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/get_offers", OffersHandler, {"config": 'Config/conf.json'}),
            (r"/api/v1/pixels", PixelListHandler),
            (r"/health-check/2ref34Dep7-el", HealthCheckHandler),
            (r"/api/v1/pixels/(?P<pixel>[^\/]+)", PixelHandler),
            (r"/api/v1/whitelisted/(?P<entity>[\w-]+)/?(?P<id>[\w-]+)?", WhitelistHandler),
            (r"/api/v1/audit", AuditHandler),
            ], cookie_secret='Q9ac2sG6D2Kos6GkHn/V8hQvwQt8S0R0kRvJ5/xj3fb=', db_hub=self.connection_hub)

    def run_server(self):
        """ Run the web server """
        logging.info('Starting web server')
        asyncio.set_event_loop(asyncio.new_event_loop())
        self.app = self.set_routes()
        self.app.listen(self.port)
        logging.info('Listening on port %d', self.port)
        tornado.ioloop.IOLoop.current().start()

    def stop_server(self):
        """ Stop the server """
        logging.info('Closing web server')
        tornado.ioloop.IOLoop.current().close()
