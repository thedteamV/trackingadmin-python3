""" BrandsPayoutHandler.py """
import logging
import json
import datetime
from tornado.web import RequestHandler
from Utils.Validator import validate_request
from Utils.ErrorHandler import handle_error
from Utils.AuditHandler import AuditHandler
import traceback
import sys


class PixelHandler(RequestHandler):

    def initialize(self):
        self.db_client = self.settings['db_hub'].get('tracking_db')
        self.audit_handler = AuditHandler(db_client=self.db_client, request=self.request)

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self, *args, **kwargs):
        offer_pixel_id = self.path_kwargs['pixel']

        try:
            offer_pixel = self._get_offer_pixel(offer_pixel_id)
            if offer_pixel is None:
                self.set_status(404)
                self.write({'message': 'Not found'})
            else:
                self.set_status(200)
                response = {"data": offer_pixel}
                self.write(json.dumps(response, indent=4, sort_keys=True, default=str))

        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(500)
            self.write(json.dumps({"message": "Error"}))


    @validate_request({
        'pixel_id': {"type": "int", "min": 0, "required": True, 'res': 'body'},
        'affiliate_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'offer_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'goal_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'trk_sys_id': {"type": "int", "min": 0, "required": True, 'res': 'body'},
        'code': {"type": "str", "required": True, 'res': 'body', 'empty': False, 'alias': 'url'},
        'type': {"type": "str", "in": ['url'], "required": True, 'res': 'body'},
        'price_per_country': {"type": "int", "required": True, 'res': 'body'},
    })
    def put(self, *args, **kwargs):
        try:
            offer_pixel_id = self.path_kwargs['pixel']
            post_data = json.loads(self.request.body)

            data = {
                'id': offer_pixel_id,
                'pixel_id': int(post_data['pixel_id']),
                'affiliate_id': 0 if post_data['affiliate_id'] == 'all' else int(post_data['affiliate_id']),
                'goal_id': 0 if post_data['goal_id'] == 'all' else int(post_data['goal_id']),
                'offer_id':0 if post_data['offer_id'] == 'all' else int(post_data['offer_id']),
                'status': 'active',
                'code': post_data['code'],
                'type': post_data['type'],
                'modified': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'trk_sys_id': post_data['trk_sys_id'],
                'all_affiliates': 1 if post_data['affiliate_id'] == 'all' else 0,
                'all_offers': 1 if post_data['offer_id'] == 'all' else 0,
                'all_goals': 1 if post_data['goal_id'] == 'all' else 0,
                'price_per_country': post_data['price_per_country'],
                'date_created': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'updated_by': self.request.headers.get('X-USER-IDENTIFY') or '',
            }

            offer_pixel = self._get_offer_pixel(offer_pixel_id)
            if offer_pixel is None:
                self.set_status(404)
                self.write({'message': 'Not found'})
            else:
                self.audit_handler.update('offerpixel', data, 'id')
                item = self._get_offer_pixel(offer_pixel_id)
                response = {"data": item}
                self.set_status(200)
                self.write(json.dumps(response, indent=4, sort_keys=True, default=str))

        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(500)
            self.write(json.dumps({"message": "Error"}))

    # soft delete
    def delete(self, *args, **kwargs):
        offer_pixel_id = self.path_kwargs['pixel']
        offer_pixel = self._get_offer_pixel(offer_pixel_id)

        try:
            if offer_pixel is None:
                self.set_status(404)
                self.write({'message': 'Not found'})
            else:
                self.audit_handler.update('offerpixel', {'status': 'deleted', 'id': offer_pixel_id}, 'id')
                item = self._get_offer_pixel(offer_pixel_id)
                self.set_status(200)
                self.write(json.dumps({"data": item}, indent=4, sort_keys=True, default=str))
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(500)
            self.write(json.dumps({"message": "Error"}))

    def _get_offer_pixel(self, offer_pixel_id):
        keys = [
            'id', 'pixel_id', 'affiliate_id', 'offer_id', 'status', 'code', 'type', 'modified',
            'goal_id', 'trk_sys_id', 'all_affiliates', 'all_offers', 'all_goals', 'price_per_country',
            'date_updated', 'date_created', 'updated_by'
        ]
        query = "SELECT {keys} FROM offerpixel WHERE id = '{id}'".format(id=offer_pixel_id, keys=','.join(keys))
        items = self.db_client.list_query(query, keys)

        if len(items) > 0:
            return items[0]

        return None