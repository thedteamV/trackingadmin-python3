""" PixelCountriesPayoutHandler.py """
import logging
import simplejson as json
import tornado.web
import traceback
import sys
from DB.Sql import Sql


class PixelCountriesPayoutHandler(tornado.web.RequestHandler):
    """ Pixel Countries payout API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.sql_client = self.settings['db_hub'].get('tracking_db')


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
        return None

    def get(self, *args, **kwargs):
        """ Get request handler. """
        self.set_custom_headers()
        results = {'status': 'Success', 'reason': ''}
        try:
            pixel_id = self.get_argument('pixel_id', default=None)
            if not pixel_id:
                result = self._get_pixels_data()
            else:
                result = self._get_pixels_country_data(pixel_id)
            results['data'] = result
            self.set_status(200)
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = str(e)
        self.write(json.dumps(results))
        return

    def post(self, *args, **kwargs):
        """ Post request handler. """
        self.set_custom_headers()
        results = {'status': 'Success', 'reason': ''}
        try:
            pixel_id = str(self.get_argument('pixel_id'))
            country_code = str(self.get_argument('country_code'))
            amount = str(self.get_argument('amount'))
            country_status = str(self.get_argument('country_status'))
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return
        try:
            comm = self.conf['pixel_country_price_update_query'].format(pixel_id=pixel_id,
                                                                        country_code=country_code,
                                                                        amount=amount,
                                                                        country_status=country_status)
            self.sql_client.command(comm)
            self.sql_client.commit()
            self.set_status(200)
            self.write(json.dumps(results))
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def delete(self, *args, **kwargs):
        """ Delete request handler. """
        self.set_custom_headers()
        results = {'status': 'Success', 'reason': ''}
        try:
            pixel_id = str(self.get_argument('pixel_id'))
            country_code = str(self.get_argument('country_code'))
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

        try:
            comm = self.conf['pixel_country_price_disable_query'].format(pixel_id=pixel_id,
                                                                         country_code=country_code)
            self.sql_client.command(comm)
            self.sql_client.commit()
            self.set_status(200)
            self.write(json.dumps(results))
        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = str(e)
            self.write(json.dumps(results))
            return

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """

    #TODO: same query like in pixelSync, refactor after DAL is implemented
    def _get_pixels_data(self):
        comm = self.conf['pixel_country_data_query']
        self.sql_client.command(comm)
        return self.sql_client.get_results_dict()

    def _get_pixels_country_data(self, pixel_id):
        comm = self.conf['pixel_country_price_query']
        comm = comm.format(pixel_id=pixel_id)
        self.sql_client.command(comm)
        return self.sql_client.get_results_dict()