""" BrandsPayoutHandler.py """
import json
import datetime
from tornado.web import RequestHandler
from Utils.Validator import validate_request
from Utils.ErrorHandler import handle_error
from Utils.Pagination import Pagination
from Utils.AuditHandler import AuditHandler
import re


class PixelListHandler(RequestHandler):

    def initialize(self):
        self.db_client = self.settings['db_hub'].get('tracking_db')
        self.audit_handler = AuditHandler(db_client=self.db_client, request=self.request)

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    @handle_error()
    @validate_request({
        "page": {"type": "int", "min": 1, "required": True, "res": "query"},
        "size": {"type": "int", "min": 10, "max": 50, "required": True, "res": "query"},
        "f[status]": {"type": "str", "in": ["active", "deleted"], "res": "query"},
        "f[affiliate_id]": {"type": "int", "min": 0, "res": "query"},
        "f[offer_id]": {"type": "int", "min": 0, "res": "query"},
        "f[goal_id]": {"type": "int", "min": 0, "res": "query"},
        "f[trk_sys_id]": {"type": "int", "min": 0, "res": "query"},
    })
    def get(self, *args, **kwargs):
        page_num = int(self.get_argument('page', default=1))
        page_size = int(self.get_argument('size', default=10))
        response = {"data": [], 'meta': {}}

        total_data = self.db_client.list_query(
            "SELECT count(*) AS total FROM tracking.offerpixel o " \
            "LEFT JOIN data.affiliates a ON a.affiliate_id = o.affiliate_id " \
            "LEFT JOIN data.offers off ON off.offer_id = o.offer_id " \
            "LEFT JOIN data.goals g ON g.goal_id = o.goal_id " \
            "LEFT JOIN data.trk_seq_ids t ON t.trk_sys_id = o.trk_sys_id " \
            "WHERE {conditions}".format(
                conditions=self._get_filters()), ['total'])
        paginator = Pagination(total_data, page_num, page_size)
        keys = [
            'o.id', 'o.pixel_id', 'o.affiliate_id', 'a.company', 'o.offer_id', 'o.status', 'o.code', 'o.type',
            'o.modified',
            'o.goal_id', 'o.trk_sys_id', 'o.all_affiliates', 'o.all_offers', 'o.all_goals', 'o.price_per_country',
            'o.date_updated', 'o.date_created', 'o.updated_by'
        ]

        query = "SELECT {keys}, off.name offer_name, g.name goal_name, t.description trk_name " \
                "FROM tracking.offerpixel o " \
                "LEFT JOIN data.affiliates a ON a.affiliate_id = o.affiliate_id " \
                "LEFT JOIN data.offers off ON off.offer_id = o.offer_id " \
                "LEFT JOIN data.goals g ON g.goal_id = o.goal_id " \
                "LEFT JOIN data.trk_seq_ids t ON t.trk_sys_id = o.trk_sys_id " \
                "WHERE {conditions} ORDER BY o.pixel_id DESC LIMIT {offset}, {limit}".format(
            conditions=self._get_filters(),
            keys=','.join(keys),
            offset=paginator.get_offset(),
            limit=page_size
        )

        items = self.db_client.list_query(query, [re.sub(r'(a\.|o\.)', '', key) for key in keys] + ['offer_name', 'goal_name', 'trk_name'])

        response['meta']['total_items'] = paginator.get_total_items()
        response['meta']['total_pages'] = paginator.get_total_pages()
        response['meta']['current_page'] = paginator.get_current_page()
        response['data'] = items

        self.set_status(200)
        self.write(json.dumps(response, indent=4, sort_keys=True, default=str))

    @handle_error()
    @validate_request({
        'affiliate_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'offer_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'goal_id': {"type": "int", "min": 0, "required": True, 'res': 'body', "except": ["all"]},
        'trk_sys_id': {"type": "int", "min": 0, "required": True, 'res': 'body'},
        'code': {"type": "str", "required": True, 'res': 'body', 'empty': False, 'alias': 'url'},
        'type': {"type": "str", "in": ['url'], "required": True, 'res': 'body'},
        'price_per_country': {"type": "int", "required": True, 'res': 'body'},
    })
    def post(self, *args, **kwargs):
        post_data = json.loads(self.request.body)
        last_pixel_id = self.db_client.list_query("SELECT max(pixel_id) max_pixel_id FROM offerpixel",
                                                      ['max_pixel_id'])

        data = {
            'pixel_id': last_pixel_id[0]['max_pixel_id']+1,
            'affiliate_id': 0 if post_data['affiliate_id'] == 'all' else int(post_data['affiliate_id']),
            'goal_id': 0 if post_data['goal_id'] == 'all' else int(post_data['goal_id']),
            'offer_id': 0 if post_data['offer_id'] == 'all' else int(post_data['offer_id']),
            'status': 'active',
            'code': post_data['code'],
            'type': post_data['type'],
            'modified': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'trk_sys_id': post_data['trk_sys_id'],
            'all_affiliates': 1 if post_data['affiliate_id'] == 'all' else 0,
            'all_offers': 1 if post_data['offer_id'] == 'all' else 0,
            'all_goals': 1 if post_data['goal_id'] == 'all' else 0,
            'price_per_country': post_data['price_per_country'],
            'date_updated': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'date_created': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'updated_by': self.request.headers.get('X-USER-IDENTIFY') or '',
        }

        offer_pixel_id = self.audit_handler.create('offerpixel', data)
        item = self._get_offer_pixel(offer_pixel_id)

        response = {"data": item}
        self.set_status(200)
        self.write(json.dumps(response, indent=4, sort_keys=True, default=str))


    def _get_filters(self):
        conditions = []
        affiliate_id_filter = self.get_argument('f[affiliate_id]', None)
        if affiliate_id_filter is not None:
            conditions.append('o.affiliate_id = {val}'.format(val=affiliate_id_filter))

        goal_id_filter = self.get_argument('f[goal_id]', None)
        if goal_id_filter is not None:
            conditions.append('o.goal_id = {val}  AND o.all_goals != 1'.format(val=goal_id_filter))

        trk_sys_id_filter = self.get_argument('f[trk_sys_id]', None)
        if trk_sys_id_filter is not None:
            conditions.append('o.trk_sys_id = {val}'.format(val=trk_sys_id_filter))

        offer_id_filter = self.get_argument('f[offer_id]', None)
        if offer_id_filter is not None:
            conditions.append('o.offer_id = {val}'.format(val=offer_id_filter))

        filter_status = self.get_argument('f[status]', None)
        if filter_status is not None:
            conditions.append("o.status = '{val}'".format(val=filter_status))

        search = self.get_argument('search', None)
        if search is not None:
            conditions.append(" ("
                              "a.company LIKE '%{val}%' "
                              "OR off.name  LIKE '%{val}%' "
                              "OR off.offer_id  LIKE '%{val}%' "
                              "OR a.affiliate_id  LIKE '%{val}%' "
                              "OR g.goal_id  LIKE '%{val}%' "
                              "OR t.trk_sys_id  LIKE '%{val}%' "
                              "OR o.code  LIKE '%{val}%' "
                              "OR o.pixel_id  LIKE '%{val}%' "
                              "OR g.name LIKE '%{val}%' "
                              "OR t.description LIKE '%{val}%')".format(val=search))

        if len(conditions) > 0:
            return ' AND '.join(conditions)

        return '1=1'

    def _get_offer_pixel(self, offer_pixel_id):
        keys = [
            'id', 'pixel_id', 'affiliate_id', 'offer_id', 'status', 'code', 'type', 'modified',
            'goal_id', 'trk_sys_id', 'all_affiliates', 'all_offers', 'all_goals', 'price_per_country',
            'date_updated', 'date_created', 'updated_by'
        ]
        query = "SELECT {keys} FROM offerpixel WHERE id = '{id}'".format(id=offer_pixel_id, keys=','.join(keys))
        items = self.db_client.list_query(query, keys)

        if len(items) > 0:
            return items[0]

        return None
