""" ConvHandler.py """
import math
import time
import logging
import json
import tornado.ioloop
import tornado.web
from DB.Sql import Sql
from Utils.AuditHandler import AuditHandler
from Utils.HelperFunct import HelperFunct


class ConvHandler(tornado.web.RequestHandler):
    """ Conv API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        self.helper_funct =  HelperFunct()
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.settings['db_hub'].get('tracking_db')


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        try:
            try:
                start = int(self.get_argument('start', default=time.time() - 60*60*24))
                end = int(self.get_argument('end', default=time.time()))
                page_num = int(self.get_argument('pageNum', default=1))
                page_size = int(self.get_argument('pageSize', default=10))
                sort_key = self.get_argument('sortKey', default='pixel_time')
                sort_direction = self.get_argument('sortDir', default='desc')
            except:
                self.set_status(400)
                return

            result = {}
            conditionals = self._get_conditionals()
            result['data'] =  self._get_conversion_rows(start, end, conditionals, sort_direction, sort_key, page_num, page_size)
            result['pagination'] = self._get_pagination_data(conditionals, end, start, page_num, page_size)
            result['status'] = 'Success'
            result['reason'] = ''
            # TODO: refactor this whole code.... order here is overriding methods
            self.set_status(200)
            self.write(json.dumps(result))
        except Exception as e:
            self.set_status(400)
            return

    def _get_conditionals(self):
        conds = ''
        for index, field in enumerate(self.conf['conv_query_fields']):
            value = self.get_argument(field, default=None)

            if value is None or value == 'None' or value == '' or value == '!':
                continue

            if value[0] == '!':
                conds = conds + ' AND ' + str(self.conf['conv_query_fields_tables'][index]) + '<>\'' + str(
                    value[1:]) + '\''
            else:
                conds = conds + ' AND ' + str(self.conf['conv_query_fields_tables'][index]) + '=\'' + str(
                    value) + '\''

        return conds

    def _get_conversion_rows(self, start, end, conditionals, sort_direction, sort_key, page_num, page_size):
        comm = self.conf['conv_query']
        comm = comm.replace('{start}', str(start))
        comm = comm.replace('{end}', str(end))
        comm = comm + conditionals
        comm = comm + " ORDER BY " + sort_key + " " + sort_direction
        if page_size:
            comm = comm + " LIMIT " + str((page_num - 1) * page_size) + ", " + str(page_size)
        self.sql_client.command(comm)
        logging.info("Select data: " + comm)
        rec = self.sql_client.get_result()
        fields = self.conf['conv_query_fields']
        results = []
        while rec:
            res = {}
            index = 0

            for field in fields:
                res[field] = str(rec[index])
                index = index + 1

            if res['status'] == '' and res['goal_id'] == '0':
                res['status'] = 'pending'
            elif res['status'] == '' and res['goal_id'] != '0':
                res['status'] = 'approved'

            results.append(res)
            rec = self.sql_client.get_result()
        return results

    def _get_pagination_data(self, conditionals, end, start, page_num, page_size):
        res = {}
        comm = self.conf['conv_query_counts']
        comm = comm.replace('{start}', str(start))
        comm = comm.replace('{end}', str(end))
        comm = comm + conditionals
        self.sql_client.command(comm)
        counts = self.sql_client.get_result()[0]
        res['total_items'] = counts
        res['page_num'] = page_num
        if page_size:
            res['num_of_pages'] = int(math.ceil(counts / float(page_size)))
        return res

    def post(self, *args, **kwargs):
        """ Post request handler. """
        self.set_custom_headers()
        request_id = self.helper_funct.setRequestID()
        audit_handler = AuditHandler(db_client=self.sql_client, request=self.request)
        answer_arr=dict()
        answer_arr['status_answer'] = 'failed'
        answer_arr['status_code'] = 0
        answer_arr['conv_id'] = -1
        answer_arr['status_tochange'] = ''
        answer_arr['message'] = ''
        answer_arr['is_amount'] = False
        answer_arr['amount'] = 0
        answer_arr['status_prev'] = ''
        answer_arr['amount_prev'] = ''

        logging.info(str(request_id) + ' route convs . ConvHandler.Post: Got request for update status pending conversions')

        try:
            conv_id = int(self.get_argument('conv_id', default=None))
            status = str(self.get_argument('status', default=None))
            amount = str(self.get_argument('amount', default='No'))
            answer_arr['conv_id'] = conv_id
            answer_arr['status_tochange'] = status
            if amount != 'No':
                try:
                    answer_arr['amount'] = float(amount)
                    answer_arr['is_amount'] = True
                except:
                    logging.warn(str(request_id) + " route convs . ConvHandler.Post: amount is wrong for update pending conversion. amount from request is " + str(amount))
                    self.write(json.dumps(answer_arr))

            if conv_id is None:
                self.set_status(400, str(request_id))
                answer_arr['status_code'] = 100
                answer_arr['message'] = "conv_id is missing for update pending conversion"
                logging.error(str(request_id) + " route convs . ConvHandler.Post: conv_id is missing for update pending conversion. answer to admin: " + json.dumps(answer_arr))
                self.write(json.dumps(answer_arr))
                return

            if status is None:
                self.set_status(400, str(request_id))
                answer_arr['status_code'] = 105
                answer_arr['message'] = 'status is missing for update pending conversion. conv_id=' + str(conv_id)
                logging.error(str(request_id) + " route convs . ConvHandler.Post: conv_id: {conv_id}. status is missing for update pending conversion. answer to admin: {answer}".format(conv_id=str(conv_id), answer=json.dumps(answer_arr)))
                self.write(json.dumps(answer_arr))
                return
    
            logging.info(str(request_id) + ' route convs . ConvHandler.Post: Got request for update status pending conversions: conv_id={conv_id}, status={status}'.format(conv_id=str(conv_id),status=status))

        except Exception as e:
            self.set_status(400, str(request_id))
            answer_arr['status_code'] = 110
            answer_arr['conv_id'] = self.get_argument('conv_id', default=None)
            answer_arr['status_tochange'] = self.get_argument('status', default=None)
            answer_arr['message'] = 'conv_id or status are wrong for update pending conversion'
            logging.error(str(request_id) + " route convs . ConvHandler.Post: Parameters are missing for update status pending conversion. answer to admin: " + json.dumps(answer_arr))
            self.write(json.dumps(answer_arr))
            return


        if status != 'pending' and status != 'approved' and status != 'rejected':
            self.set_status(400, str(request_id))
            answer_arr['status_code'] = 115
            answer_arr['message'] = 'status type is wrong for update pending conversion'
            logging.error(str(request_id) + " route convs . ConvHandler.Post: status type is wrong for update pending conversion. answer to admin: " + json.dumps(answer_arr))
            self.write(json.dumps(answer_arr))
            return

        try:
            comm = self.conf['conv_update_select_conversion'].format(conv_id)
            self.sql_client.command(comm)
            result = self.sql_client.get_result()
            if result != None:
                answer_arr['status_prev'] = result[0]
                answer_arr['amount_prev'] = result[1]
            else:
                self.set_status(401, str(request_id))
                answer_arr['status_code'] = 120
                answer_arr['message'] = 'conv_id ' + str(conv_id) + ' not found in pending conversions. Update was failed'
                logging.error(str(request_id) + " route convs . ConvHandler.Post: . answer to admin: conv_id {conv_id} not found in pending conversions. Update was failed. answer to admin: {answer} ".format(conv_id=str(conv_id), answer=json.dumps(answer_arr)))
                self.write(json.dumps(answer_arr))
                return
        except Exception as e:
            self.set_status(402, str(request_id))
            answer_arr['status_code'] = 120
            answer_arr['message'] = 'conv_id ' + str(conv_id) + ' not found in pending conversions. Update was failed with error ' + str(e)
            logging.error(str(request_id) + " route convs . ConvHandler.Post: . answer to admin: conv_id {conv_id} not found in pending conversions. Update was failed. answer to admin: {answer} ".format(conv_id=str(conv_id), answer=json.dumps(answer_arr)))
            self.write(json.dumps(answer_arr))
            return

        try:
            if answer_arr['is_amount'] == True:
                comm = self.conf['conv_update_with_amount'].format(status, answer_arr['amount'],conv_id)
            else:    
                comm = self.conf['conv_update'].format(status, conv_id)

            self.sql_client.command(comm)
            self.sql_client.commit()
            if self.sql_client.mCursor.rowcount == 0:
                self.set_status(403, str(request_id))
                answer_arr['status_code'] = 120
                answer_arr['message'] = 'conv_id ' + str(conv_id) + ' no changes for pending conversions. Update was canceled'
                logging.error(str(request_id) + " route convs . ConvHandler.Post: . answer to admin: conv_id {conv_id} not found in pending conversions. Update was failed. answer to admin: {answer} ".format(conv_id=str(conv_id), answer=json.dumps(answer_arr)))
                self.write(json.dumps(answer_arr))
                return

            self.set_status(200, str(request_id))
            answer_arr['status_code'] = 200
            answer_arr['status_answer'] = 'success'
            answer_arr['message'] = 'conv_id = ' + str(conv_id) + ' was successfully updated to status ' + status
            logging.info(str(request_id) + " route convs . ConvHandler.Post: conv_id: {conv_id} updated for pending conversion to status {status} . answer to admin: {answer} ".format(conv_id=str(conv_id), status=status, answer=json.dumps(answer_arr)))

            new_data = {'id': conv_id,
                        'status' : answer_arr['status_tochange'],
                        'amount' : answer_arr['amount'],
                        'is_amount' : answer_arr['is_amount']

            }
            old_data = {'id': conv_id,
                        'status' : answer_arr['status_prev'],
                        'amount' : answer_arr['amount_prev']
            }
            audit_handler._save_audit('conversions', 'update', conv_id, new_data, old_data)
            self.write(json.dumps(answer_arr))
            return

        except Exception as e:
            logging.error(str(e))
            self.set_status(400, str(request_id))
            answer_arr['status_code'] = 120
            answer_arr['message'] = 'Cannot update status for pending conversion'
            logging.error(str(request_id) + " route convs . ConvHandler.Post: conv_id: {conv_id} Cannot update status for pending conversion. answer to admin: " + json.dumps(answer_arr))
            self.write(json.dumps(answer_arr))
            return


    def save_history_toaudit (self, conv_id, answer_arr):
        """ save pending conv to audit """

        new_data = {'id': conv_id,
                    'status' : answer_arr['status'],
                    'amount' : answer_arr['amount'],
                    'is_amount' : answer_arr['is_amount']

        }
        old_data = {'id': conv_id,
                    'status' : answer_arr['status_prev'],
                    'amount' : answer_arr['amount_prev']
        }

    def put(self, *args, **kwargs):
        """ Put request handler. """

    def delete(self, *args, **kwargs):
        """ Delete request handler. """

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """
