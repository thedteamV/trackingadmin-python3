""" CountriesPayoutHandler.py """
import logging
import simplejson as json
import tornado.web
from DB.Sql import Sql


class CountriesPayoutHandler(tornado.web.RequestHandler):
    """ Countries payout API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.settings['db_hub'].get('tracking_db')

    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        self.set_custom_headers()
        results = {'status': 'Success', 'reason': ''}
        try:
            results['data'] = self._get_country_code_to_amount()
            self.set_status(200)
        except Exception as e:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = str(e)
        self.write(json.dumps(results))
        return

    def post(self, *args, **kwargs):
        """ Post request handler. """
        self.set_custom_headers()
        results = {'status': 'Success', 'reason': ''}
        try:
            country_id = str(self.get_argument('country_id'))
            payout = float(self.get_argument('payout'))
            country_status = str(self.get_argument('country_status'))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return
        try:
            comm = self.conf['country_update_query'].format(country_id=country_id,
                                                            amount=payout,
                                                            country_status=country_status)
            self.sql_client.command(comm)
            self.sql_client.commit()

            results['status'] = 'Success'
            results['reason'] = ''
            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """

    #TODO: same query like in pixelSync, refactor after DAL is implemented
    def _get_country_code_to_amount(self):
        comm = self.conf['country_data_query']
        self.sql_client.command(comm)
        return self.sql_client.get_results_dict()