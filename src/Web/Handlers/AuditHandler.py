""" BrandsPayoutHandler.py """
from tornado.web import RequestHandler
from Utils.Validator import validate_request
from Utils.Pagination import Pagination
import traceback
import logging
import json
import sys


class AuditHandler(RequestHandler):

    def initialize(self):
        self.db_client = self.settings['db_hub'].get('tracking_db')

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    @validate_request({
        "page": {"type": "int", "min": 1, "required": True, "res": 'query'},
        "size": {"type": "int", "min": 10, "max": 50, "required": True, "res": 'query'}
    })
    def get(self, *args, **kwargs):
        page_num = int(self.get_argument('page', default=1))
        page_size = int(self.get_argument('size', default=10))
        response = {"data": [], 'meta': {}}

        try:
            total_data = self.db_client.list_query("SELECT count(*) AS total FROM tracking.audits", ['total'])
            paginator = Pagination(total_data, page_num, page_size)
            keys = [
                'id', 'user_id', 'event', 'auditable_type',
                'auditable_id', 'old_values', 'new_values', 'url',
                'ip_address', 'user_agent', 'tags', 'created_at'
            ]

            query = "SELECT {keys} FROM tracking.audits ORDER BY created_at DESC LIMIT {offset}, {limit}".format(
                keys=",".join(keys),
                offset=paginator.get_offset(),
                limit=page_size
            )
            items = self.db_client.list_query(query, keys)

            response['meta']['total_items'] = paginator.get_total_items()
            response['meta']['total_pages'] = paginator.get_total_pages()
            response['meta']['current_page'] = paginator.get_current_page()
            response['data'] = items

            self.set_status(200)
            self.write(json.dumps(response, indent=4, sort_keys=True, default=str))

        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            self.set_status(500)
            self.write(json.dumps({"message": "Error"}))
