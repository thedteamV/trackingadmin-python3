""" BrandsPayoutReportHandler.py """
import time
import logging
import json
import tornado.ioloop
import tornado.web
from DB.Sql import Sql


class BrandsPayoutReportHandler(tornado.web.RequestHandler):
    """ Brands payout report API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.settings['db_hub'].get('tracking_db')


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        self.set_custom_headers()

        try:
            comm = self.conf['brands_payout_query']
            self.sql_client.command(comm)
            rec = self.sql_client.get_result()

            fields = self.conf['brands_payout_query_fields']

            results = dict()
            results['data'] = list()

            while rec is not None:
                res = dict()
                index = 0

                for field in fields:
                    res[field] = str(rec[index])
                    index = index + 1

                results['data'].append(res)
                rec = self.sql_client.get_result()

            results['status'] = 'Success'
            results['reason'] = ''

            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def post(self, *args, **kwargs):
        """ Post request handler. """
        self.set_status(400)
        return

    def put(self, *args, **kwargs):
        """ Put request handler. """
        self.set_status(400)
        return

    def delete(self, *args, **kwargs):
        """ Delete request handler. """
        self.set_status(400)
        return

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """
