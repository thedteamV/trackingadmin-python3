""" DefaultHandler.py """
import simplejson as json
import tornado.ioloop
import tornado.web

class HealthCheckHandler(tornado.web.RequestHandler):
    """ Default API handler """
    def initialize(self):
        """ Initializes the handler. """

    def get(self, *args, **kwargs):
        """ Get request handler. """
        response = dict()

        response['status'] = 'success'
        self.write(json.dumps(response))
