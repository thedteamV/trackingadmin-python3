""" BrandsPayoutHandler.py """
import time
import logging
import json
import tornado.ioloop
import tornado.web
from DB.Sql import Sql


class BrandsPayoutHandler(tornado.web.RequestHandler):
    """ Brands payout API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.settings['db_hub'].get('tracking_db')


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        self.set_custom_headers()

        try:
            comm = self.conf['brands_payout_query']
            self.sql_client.command(comm)
            rec = self.sql_client.get_result()

            fields = self.conf['brands_payout_query_fields']

            results = dict()
            results['data'] = list()

            while rec is not None:
                res = dict()
                index = 0

                for field in fields:
                    res[field] = str(rec[index])
                    index = index + 1

                results['data'].append(res)
                rec = self.sql_client.get_result()

            results['status'] = 'Success'
            results['reason'] = ''

            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def post(self, *args, **kwargs):
        """ Post request handler. """
        self.set_custom_headers()

        try:
            results = dict()

            try:
                brand_name = str(self.get_argument('brand_name', default=None))
                country_code = str(self.get_argument('country_code', default=None))
                payout = float(self.get_argument('payout', default=-999))
            except:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            comm = self.conf['brands_payout_insert'].format('\'' + str(brand_name) + '\'', '\'' + str(country_code) + '\'', '\'' + str(payout) + '\'')
            self.sql_client.command(comm)
            self.sql_client.commit()

            results['status'] = 'Success'
            results['reason'] = ''
            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def put(self, *args, **kwargs):
        """ Put request handler. """
        self.set_custom_headers()

        try:
            results = dict()

            try:
                row_id = int(self.get_argument('id', default=-1))
                brand_name = str(self.get_argument('brand_name', default=None))
                country_code = str(self.get_argument('country_code', default=None))
                payout = float(self.get_argument('payout', default=-999))
            except:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            if row_id == -1:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            if brand_name != 'None':
                comm = self.conf['brands_payout_update'].format('brand_name', '\'' + str(brand_name) + '\'', '\'' + str(row_id) + '\'')
            elif country_code != 'None':
                comm = self.conf['brands_payout_update'].format('country_code', '\'' + str(country_code) + '\'', '\'' + str(row_id) + '\'')
            elif payout != -999:
                comm = self.conf['brands_payout_update'].format('payout', '\'' + str(payout) + '\'', '\'' + str(row_id) + '\'')
            else:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            self.sql_client.command(comm)
            self.sql_client.commit()

            results['status'] = 'Success'
            results['reason'] = ''
            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def delete(self, *args, **kwargs):
        """ Delete request handler. """
        self.set_custom_headers()

        try:        
            results = dict()
            
            try:
                row_id = int(self.get_argument('id', default=-1))
            except:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            if row_id == -1:
                self.set_status(400)
                results['status'] = 'Failure'
                results['reason'] = 'Bad arguments'
                self.write(json.dumps(results))
                return

            comm = self.conf['brands_payout_delete'].format(row_id)
            self.sql_client.command(comm)
            self.sql_client.commit()

            results['status'] = 'Success'
            results['reason'] = ''
            self.set_status(200)
            self.write(json.dumps(results))
        except:
            self.set_status(400)
            results['status'] = 'Failure'
            results['reason'] = 'Bad arguments'
            self.write(json.dumps(results))
            return

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """
