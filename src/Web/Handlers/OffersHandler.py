""" BrandsPayoutHandler.py """
import time
import logging
import json
import tornado.ioloop
import tornado.web
from DB.Sql import Sql


class OffersHandler(tornado.web.RequestHandler):
    """ Offers handler API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()
        self.sql_client = self.settings['db_hub'].get('tracking_db')

    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        results = dict()
        self.set_custom_headers()

        offer_id = self.get_argument('offer_id', default=None)
        advertiser_id = self.get_argument('advertiser_id', default=None)

        query = self.conf['get_offers_query']

        if offer_id is not None or advertiser_id is not None:
            query += ' WHERE '

            if offer_id is not None:
                query += 'offer_id=' + offer_id

                if advertiser_id is not None:
                    query += ' AND '

            if advertiser_id is not None:
                query += 'advertiser_id=' + advertiser_id

        results['data'] = self.sql_client.list_query(query, self.conf['get_offers_query_fields'])
        results['status'] = 'true'
        
        self.set_status(200)
        self.write(json.dumps(results))
        
        return



    def post(self, *args, **kwargs):
        """ Post request handler. """

    def put(self, *args, **kwargs):
        """ Put request handler. """

    def delete(self, *args, **kwargs):
        """ Delete request handler. """

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """
