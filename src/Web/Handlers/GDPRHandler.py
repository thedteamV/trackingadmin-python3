""" BrandsPayoutHandler.py """
import time
import logging
import json
import tornado.ioloop
import tornado.web
from DB.Sql import Sql


class GDPRHandler(tornado.web.RequestHandler):
    """ Brands payout API handler """
    def initialize(self, config):
        """ Initializes the handler. """
        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        self.sql_client = self.settings['db_hub'].get('tracking_db')


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')

        return None


    def get(self, *args, **kwargs):
        """ Get request handler. """
        self.set_custom_headers()

        access_key = str(self.get_argument('access_key', default=None))
        user_email = str(self.get_argument('user_email', default=None))

        if access_key is None or access_key == 'None' or access_key != self.conf['gdpr_access_key']:
            logging.error('GDPR error deleting user: {}'.format(user_email))
            self.set_status(403)
            return

        logging.info('GDPR user deleted: {}'.format(user_email))
        self.set_status(200)
        return



    def post(self, *args, **kwargs):
        """ Post request handler. """

    def put(self, *args, **kwargs):
        """ Put request handler. """

    def delete(self, *args, **kwargs):
        """ Delete request handler. """

    def options(self, *args, **kwargs):
        """ Options request handler. """
        self.set_status(200)
        self.set_custom_headers()
        self.add_header('Access-Control-Max-Age', '3000000')
        return

    def data_received(self, chunk):
        """ Data received handler. """
