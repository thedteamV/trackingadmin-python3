""" ESPDistHandler.py """
import json
import logging
import tornado.web
from redis import Redis

from DB.Sql import Sql
from ESP.ListsManager import ListsManager


class ESPDistHandler(tornado.web.RequestHandler):
    """ ESPDistHandler API handler """
    def initialize(self, env_config, config, esp_config):
        """ Initializes the handler. """

        with open(config, 'rb') as json_file:
            self.conf = json.load(json_file)
            json_file.close()

        try:
            self.sql_client = Sql()
            self.sql_client.set_parameters(User=env_config['sql_user'],
                                           Password=env_config['sql_pass'],
                                           Host=env_config['sql_host'],
                                           Port=env_config['sql_port'],
                                           Database=env_config['sql_db'])
            self.sql_client.connect()
            logging.info('ESPDistHandler is connected to MySql')

        except:
            logging.error('ESPDistHandler could not connect to MySql DB')
            return False

        try:
            self.redis_client = Redis(host=self.conf['redis_host'],
                                      password=self.conf['redis_pass'])
            logging.info('ESPDistHandler is connected to Redis')

        except Exception as e:
            logging.error('ESPDistHandler could not connect to Redis DB')
            return False


        try:
            self.list_manager = ListsManager(name='ListCaching',
                                             sql_client=self.sql_client,
                                             redis_client=self.redis_client,
                                             config_file=config,
                                             esp_config_file=esp_config)
        except:
            logging.error('ESPDistHandler could not init Lists Manager')
            exit()


    def set_custom_headers(self):
        """ Sets few headers. """
        try:
            self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin'))
        except:
            self.set_status(403)
            return

        self.add_header('Access-Control-Allow-Credentials', 'true')
        self.add_header('Access-Control-Allow-Headers', 'content-type')
        self.add_header('Access-Control-Allow-Methods', 'PUT')

        return None


    def put(self, *args, **kwargs):
        """ Put request handler. """
        try:
            self.list_manager.cache_esp_lists_list()
            self.set_status(200)
        except Exception as e:
            self.set_status(400)
        return