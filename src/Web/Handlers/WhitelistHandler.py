from tornado.web import RequestHandler
from Utils.Validator import validate_request
from Utils.ErrorHandler import handle_error
from Utils.AuditHandler import AuditHandler
from Utils.Pagination import Pagination
import datetime
import json


class WhitelistHandler(RequestHandler):

    def initialize(self):
        self.db_client = self.settings['db_hub'].get('tracking_db')
        self.audit_handler = AuditHandler(db_client=self.db_client, request=self.request)

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    @handle_error()
    @validate_request({
        "entity": {"type": "str", "in": ['affiliates', 'offers', 'trk_sys_ids'], "required": True, "res": "path"}
    })
    def get(self, *args, **kwargs):
        entity_type = self.path_kwargs['entity']
        items = self._get(entity_type)

        response = {'data': items}

        self.set_status(200)
        self.write(json.dumps(response, indent=4, sort_keys=True, default=str))

    @handle_error()
    @validate_request({
        "fto": {"type": "int", "min": 0, "required": True, "res": "body"},
        "fte_pa": {"type": "int", "min": 0, "required": True, "res": "body"},
        "ftc": {"type": "int", "min": 0, "required": True, "res": "body"},
        "ftd": {"type": "int", "min": 0, "required": True, "res": "body"},
        "id": {"type": "int", "min": -1, "required": True, "res": "path"},
        "entity": {"type": "str", "in": ['affiliates', 'offers', 'trk_sys_ids'], "required": True, "res": "path"}
    })
    def post(self, *args, **kwargs):
        entity_type = self.path_kwargs['entity']
        entity_id = self.path_kwargs['id']
        post_data = json.loads(self.request.body)

        res = self._get_one(entity_type, entity_id)
        if res is not None:
            self.set_status(400)
            self.write(json.dumps({"message": "Bad Request.", "errors": {'id': ['The id already exists.']}}))
            return

        item = self._create(entity_type, entity_id, post_data)

        if item is None:
            self.set_status(404)
            self.write({'message': 'Not found'})
        else:
            self.set_status(200)
            self.write(json.dumps({"data": item}, indent=4, sort_keys=True, default=str))

    @handle_error()
    @validate_request({
        "fto": {"type": "int", "min": 0, "required": True, "res": "body"},
        "fte_pa": {"type": "int", "min": 0, "required": True, "res": "body"},
        "ftc": {"type": "int", "min": 0, "required": True, "res": "body"},
        "ftd": {"type": "int", "min": 0, "required": True, "res": "body"},
        "id": {"type": "int", "min": -1, "required": True, "res": "path"},
        "entity": {"type": "str", "in": ['affiliates', 'offers', 'trk_sys_ids'], "required": True, "res": "path"}
    })
    def put(self, *args, **kwargs):
        post_data = json.loads(self.request.body)
        entity_type = self.path_kwargs['entity']
        entity_id = self.path_kwargs['id']

        res = self._get_one(entity_type, entity_id)
        if res is None:
            self.set_status(400)
            self.write(json.dumps({"message": "Bad Request.", "errors": {'id': ['The id does not exists.']}}))
            return

        item = self._update(entity_type, entity_id, post_data)

        if item is None:
            self.set_status(404)
            self.write({'message': 'Not found'})
        else:
            self.set_status(200)
            self.write(json.dumps({"data": item}, indent=4, sort_keys=True, default=str))

    @handle_error()
    @validate_request({
        "id": {"type": "int", "min": -1, "required": True, "res": "path"},
        "entity": {"type": "str", "in": ['affiliates', 'offers', 'trk_sys_ids'], "required": True, "res": "path"}
    })
    def delete(self, *args, **kwargs):
        entity_type = self.path_kwargs['entity']
        entity_id = self.path_kwargs['id']

        res = self._get_one(entity_type, entity_id)
        if res is None:
            self.set_status(400)
            self.write(json.dumps({"message": "Bad Request.", "errors": {'id': ['The id does not exists.']}}))
            return

        item = self._delete(entity_type, entity_id)

        if item is None:
            self.set_status(404)
            self.write({'message': 'Not found'})
        else:
            self.set_status(200)
            self.write(json.dumps({"data": item}, indent=4, sort_keys=True, default=str))

    def _det_settings_by_type(self, entity_type):
        if entity_type == 'trk_sys_ids':
            return {
                'table': 'whitelist_trk_sys',
                'id': 'trk_sys_id',
            }

        if entity_type == 'offers':
            return {
                'table': 'whitelist_offers',
                'id': 'offer_id',
            }

        if entity_type == 'affiliates':
            return {
                'table': 'whitelist_affiliates',
                'id': 'affiliate_id',
            }

    def _get_one(self, entity_type, entity_id):
        settings = self._det_settings_by_type(entity_type)
        keys = [settings['id'], 'fto', 'ftd', 'ftc', 'created_at', 'updated_at']
        query = "SELECT {keys} FROM {table} WHERE {key} = {val}".format(
            keys=','.join(keys),
            table=settings['table'],
            key=settings['id'],
            val=entity_id
        )
        items = self.db_client.list_query(query, keys)

        if len(items) > 0:
            return items[0]

        return None

    def _get(self, entity_type):
        settings = self._det_settings_by_type(entity_type)

        if entity_type == 'affiliates':
            keys = [settings['id'], 'company', 'fto', 'ftd', 'ftc', 'fte_pa', 'created_at', 'updated_at']
            query = "SELECT o.affiliate_id, a.company, o.fto, o.ftd, o.ftc, fte_pa, o.created_at, o.updated_at FROM {table} o LEFT JOIN data.affiliates a ON a.affiliate_id = o.affiliate_id  WHERE deleted_at IS NULL ORDER BY updated_at DESC".format(
                table=settings['table']
            )
        elif entity_type == 'offers':
            keys = [settings['id'], 'name', 'fto', 'ftd', 'ftc', 'fte_pa', 'created_at', 'updated_at']
            query = "SELECT o.offer_id, a.name, o.fto, o.ftd, o.ftc, fte_pa, o.created_at, o.updated_at FROM {table} o LEFT JOIN data.offers a ON a.offer_id = o.offer_id  WHERE deleted_at IS NULL ORDER BY updated_at DESC".format(
                table=settings['table']
            )
        elif entity_type == 'trk_sys_ids':
            keys = [settings['id'], 'description', 'fto', 'ftd', 'ftc', 'fte_pa', 'created_at', 'updated_at']
            query = "SELECT o.trk_sys_id, a.description, o.fto, o.ftd, o.ftc, fte_pa, o.created_at, o.updated_at FROM {table} o LEFT JOIN data.trk_seq_ids a ON a.trk_sys_id = o.trk_sys_id  WHERE deleted_at IS NULL ORDER BY updated_at DESC".format(
                table=settings['table']
            )

        return self.db_client.list_query(query, keys)

    def _create(self, entity_type, entity_id, data):
        settings = self._det_settings_by_type(entity_type)
        data[settings['id']] = entity_id
        data['created_at'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        data['updated_at'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        entity_id = self.audit_handler.create(settings['table'], data, auditable_id=data[settings['id']])
        return self._get_one(entity_type, entity_id)

    def _update(self, entity_type, entity_id, data):
        settings = self._det_settings_by_type(entity_type)
        data[settings['id']] = entity_id
        data['updated_at'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.audit_handler.update(settings['table'], data, settings['id'])
        return self._get_one(entity_type, entity_id)

    def _delete(self, entity_type, entity_id):
        settings = self._det_settings_by_type(entity_type)
        data = {'fto': '', 'ftd': '', 'ftc': '', 'fte_pa': '', 'created_at': '', 'updated_at': '', settings['id']: entity_id}
        self.audit_handler.delete(settings['table'], data, entity_id, settings['id'])
        return {}
