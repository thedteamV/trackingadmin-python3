""" Log.py """
import logging

class Log(object):
    """ Log class """
    def __init__(self, name='Module', logfile='server.log'):
        """ Constructor. """
        self.name = name
        self.logfile = logfile

    def config(self):
        """ Configure the logger. """
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(threadName)-12s %(levelname)-7s  %(message)s',
                            datefmt='%y-%m-%d %H:%M:%S')

    def banner(self):
        """ Prints start banner. """
        logging.info('************************************************************')
        logging.info('Starting new %s session', self.name)
        logging.info('************************************************************')

    def start(self):
        """ Starts the logger. """
        self.config()
        self.banner()
