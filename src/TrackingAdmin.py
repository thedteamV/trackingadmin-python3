""" TrackingAdmin.py """
import threading
from Log.Log import Log
from queue import Queue
from Service.PixelSync import PixelSync
from Service.OptinValidation import OptinValidation
from Service.EspDist import ESPDist
from Service.ESPDistChangeHandler import ESPDistChangeHandler
from Service.Push77Dist import Push77Dist
from Web.Server.WebServer import WebServer
from Service.IndirectSync import IndirectSync
import time
import os


class TrackingAdmin(object):
    """ Tracking admin class """

    def __init__(self):
        """ Constructor. """
        threading.current_thread().name = 'TrackingAdmin'
        self.log = Log(name='TrackingAdmin', logfile='/var/log/tracking/tracking.log')
        self.log.start()


    def run(self):
        """ Starts the messager. """
        q = Queue()

        # Pixels sync
        self.pixels_sync = PixelSync(name='PixelSync', config_file='Config/conf.json', local_config_file='Config/local_config.json')
        self.pixels_sync.start()

        # Optins check
        self.optin_validation = OptinValidation(name='OptinValidation', config_file='Config/conf.json')
        self.optin_validation.start()

        # Push77
        self.esp_dist = Push77Dist(name='Push77Dist', config_file='Config/conf.json', esp_config_file='Config/esp_conf.json')
        self.esp_dist.start()

        # ESP dist thread handler
        time.sleep(1)
        self.esp_dist_change_handler = ESPDistChangeHandler(name='ESPDistChangeHandler', data_config_file='Config/data_conf.json')
        self.esp_dist_change_handler.set_queues("detect_ui_change", q, q)
        self.esp_dist_change_handler.start()

        # ESP dist
        time.sleep(1)
        self.esp_dist = ESPDist(name='ESPDist', config_file='Config/conf.json', esp_config_file='Config/esp_conf.json')
        self.esp_dist.set_queues("detect_ui_change", q, q)
        self.esp_dist.start()

        # Indirect sync
        time.sleep(1)
        self.indirect_sync = IndirectSync(name='IndirectSync', config_file='Config/conf.json', local_config_file='Config/local_config.json')
        self.indirect_sync.start()

        # Web Server
        time.sleep(3)
        self.web_server = WebServer(port=80)
        self.web_server.start()
