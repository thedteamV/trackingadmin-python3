""" Sendlane.py """
import logging
import json
import requests
from ESP.ESPBase import ESPBase

class SendlaneForm(ESPBase):
    """ Sendlane class """

    def __init__(self, name='Sendlane', config_file=None, esp_config_file=None, list_id=0, network_name=''):
        """ Constructor. """
        self.name = name
        self.network_name = network_name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        #ESPBase.__init__(self, self.name, self.config_file, self.esp_config_file, self.list_id)

        self.api_key = None
        self.hash = None
        self.net_list_id = None
        self.account_name = None
        self.owner = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        add_contact_request = self.esp_config['sendlane_form_add_contacts'] + str(contact['net_list_id'])
        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}

        body = {'1': contact['first_name'],
                '2': contact['last_name'],
                '3': contact['email']
                }
        if self.network_name == 'CCUPLOAD' or self.network_name == 'KOI' or self.network_name == 'CIT':
            logging.info('Added clickid')
            body['Clickid'] = contact['paramvar_4']

        response_data = requests.post(add_contact_request, headers=headers, data=body, timeout=60)

        if response_data.status_code != 200:
            logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s Error code: %s',
                          self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
            return False

        logging.info('Contact added successfuly. Network: %s List ID: %s User: %s ', self.name, str(self.list_id), str(contact['email']))

        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        for contact in contacts_list:
            added = self.add_contact(contact)
            if added:
                logging.info('Contacts added successfuly. Network: %s List ID: %s', self.name, str(self.list_id))
            else:
                logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s',self.name, str(self.list_id), str(contact['email']))

        return True
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """
        return self.add_contact(self.list_id, contact)

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """

    def delete_contact(self, contact):
        """ Delete contact from a given list. """

    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        return True
