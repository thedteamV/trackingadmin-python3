""" Ongage.py """
import logging
import json
import requests
from ESP.ESPBase import ESPBase

class Ongage(ESPBase):
    """ Ongage class """

    def __init__(self, name='Ongage', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        #ESPBase.__init__(self, self.name, self.config_file, self.esp_config_file, self.list_id)

        self.user = None
        self.password = None
        self.api_key = None
        self.hash = None
        self.net_list_id = None
        self.account_name = None
        self.owner = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        contacts_list = list()
        contacts_list.append(contact)
        return self.add_contacts(contacts_list)

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        add_contacts_request = self.esp_config['ongage_add_contacts']
        add_contacts_request = add_contacts_request.replace('{list_id}', self.net_list_id)

        # Sets the headers.
        headers_dict = dict()
        headers_dict['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        headers_dict['X_USERNAME'] = str(self.user)
        headers_dict['X_PASSWORD'] = str(self.password)
        headers_dict['X_ACCOUNT_CODE'] = str(self.api_key)
        headers_dict['Content-Type'] = 'application/json'

        headers = headers_dict

        # Sets the body.
        body_list = list()
        for contact in contacts_list:
            contact_dict = dict()
            contact_dict['email'] = contact['email']
            contact_dict['first_name'] = contact['first_name']
            contact_dict['last_name'] = contact['last_name']
            contact_dict['country'] = contact['country_code']
            body_list.append(contact_dict)

        for i in range(0, len(body_list), self.esp_config['ongage_max_chunk']):
            body = json.dumps(body_list[i:i+self.esp_config['ongage_max_chunk']])
            print(len(body))
            logging.info ('Sending data to Ongage. body: '+ body)
            response_data = requests.post(add_contacts_request, headers=headers, data=body, timeout=5)
            
            logging.info ('Response from Ongage: '+ str(response_data))
            # Checks the response.
            if response_data == 200:
                logging.error('Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
                continue

            if response_data.status_code == 412:
                continue

            if response_data.status_code != 200:
                logging.error('Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
                continue

            if 'metadata' in response_data.keys():
                if 'error' in response_data['metadata'].keys():
                    if response_data['metadata']['error'] != 'false':
                        try:
                            logging.error('Error adding contacts to network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
                            continue
                        except:
                            continue

        logging.info('Contacts added successfuly. Network: %s List ID: %s', self.name, str(self.list_id))

        return True
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """
        return False

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """
        return False


    def delete_contact(self, list_id, contact):
        """ Delete contact from a given list. """
        return False


    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.api_key is not None and self.user is not None and \
           self.password is not None and self.net_list_id is not None:
            return True

        # Checks the DB for active connection.
        account_details_query = self.esp_config['account_details_query']
        account_details_query = account_details_query.replace('{list_id}', str(self.list_id))
        account_details_query_fields = self.esp_config['account_details_query_fields']
        
        try:
            account_details = self.sql_client.list_query(account_details_query, account_details_query_fields)[0]
        except:
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            return False

        self.user = account_details['user']
        self.password = account_details['pass']
        self.api_key = account_details['api_key']
        self.hash = account_details['hash']
        self.net_list_id = account_details['net_list_id']
        self.list_name = account_details['list_name']
        self.network_name = account_details['network_name']
        self.owner = account_details['owner']

        # In case we had all the details in the DB.
        if self.api_key is not None and self.user is not None and \
           self.password is not None and self.net_list_id is not None:
            return True

        logging.error('Error getting network data from DB. Network: %s List ID: %s', self.name, str(self.list_id))

        return False
