""" PushTracker.py """
import time
import logging
import json
import requests
from ESP.ESPBase import ESPBase

class PushTracker(ESPBase):
    """ PushTracker class """

    def __init__(self, name='PushTracker', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        ESPBase.__init__(self, self.name, self.config_file, self.esp_config_file, self.list_id)

        self.api_key = None
        self.hash = None
        self.net_list_id = None
        self.account_name = None
        self.owner = None

    def connect(self):
        """ ESP connection. """
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """

        #check if have to login
        self.connect()

        push_url = ''
        if self.esp_config['pushtrack_status'] == 'prod':
            push_url = self.esp_config['pushtrack_domain_prod'] + self.esp_config['pushtrack_addmessage']
        else:
            push_url = self.esp_config['pushtrack_domain_dev'] + self.esp_config['pushtrack_addmessage']

        push_url = push_url.replace('{message_id}', contact['message_id'])

        # Sets the headers.
        headers_dict = dict()
        headers_dict['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        headers_dict['Authorization'] = 'Bearer ' + str(self.api_key)
        headers_dict['Content-Type'] = 'application/json'
        headers = headers_dict

        contact_dict = dict()
        contact_dict['action'] = contact['conv_type'] 
        if contact['conv_type'] == 'Lead' or contact['conv_type'] == 'Optin':
            contact_dict['type'] = 'countable'

        if contact['conv_type'] == 'FTD' or contact['conv_type'] == 'Deposit':    
            contact_dict['type'] = 'valuable'
            contact_dict['currency_code'] = 'USD'
            contact_dict['value'] = str(contact['amount'])

        if contact['conv_type'] == 'Event':
            if contact['event'] == 'PA':
                contact_dict['type'] = 'countable'
                contact_dict['action'] = 'Event'
            else:
                contact_dict['type'] = 'valuable'
                contact_dict['currency_code'] = 'USD'
                contact_dict['value'] = str(contact['amount'])
                contact_dict['action'] = 'Deposit'

        body = json.dumps(contact_dict)


        logging.info ('Sending data to Push77 for conversion_id=' + str(contact['conversion_id']) + ': '+ body)
        response_data = requests.post(push_url, headers=headers, data=body, timeout=5)
        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        for contact in contacts_list:
            self.add_contact(contact)
    
        return True
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """
        return True

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """
        return True

    def delete_contact(self, contact):
        """ Delete contact from a given list. """
        return True


    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.api_key is not None and self.hash is not None and self.net_list_id is not None and int(self.hash) > int(time.time()):
            return True

        # Checks the DB for active connection.
        account_details_query = self.esp_config['push_account_details_query']
        #account_details_query = account_details_query.replace('{list_id}', str(self.list_id))
        account_details_query_fields = self.esp_config['push_account_details_query_fields']
        
        try:
            account_details = self.sql_client.list_query(account_details_query, account_details_query_fields)[0]
        except:
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            return False

        self.api_key = account_details['api_key']
        self.hash = account_details['hash']
        self.network_id = account_details['network_id']
        #self.net_list_id = account_details['net_list_id']
        #self.list_name = account_details['list_name']
        #self.network_name = account_details['network_name']
        self.owner = account_details['owner']

        # In case we had all the details in the DB.
        time_now = int(time.time())
        if self.api_key is not None and self.api_key != '' and \
           self.hash is not None and self.hash != '' and \
           int(self.hash) > time_now:
            return True

        # No active connection in the DB, try to get it from the network itself.
        if account_details['user'] != '' and account_details['pass'] != '' and \
           account_details['user'] is not None and account_details['pass'] is not None:
            connect_request = ''
            if self.esp_config['pushtrack_status'] == 'prod':
                connect_request = self.esp_config['pushtrack_domain_prod'] + self.esp_config['pushtrack_login']
            else:
                connect_request = self.esp_config['pushtrack_domain_dev'] + self.esp_config['pushtrack_login']

            connect_request = connect_request.replace('{api_key}', account_details['user'])
            connect_request = connect_request.replace('{secret}', account_details['pass'])

            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
            response_data = requests.post(connect_request, headers=headers, timeout=5)
            
            if response_data.status_code != 200:
                logging.error('Could not retrieve api key and hash from network: %s List ID: %s Error code: %s', self.name, str(self.list_id), str(response_data.status_code))
                return False

            response = json.loads(response_data.text)

            self.api_key = response['token']
            self.hash = int(time.time() + 3600)

            # Updates the DB with the new details.
            account_details_query = self.esp_config['push_account_update_details']
            account_details_query = account_details_query.replace('{api_key}', str(self.api_key))
            account_details_query = account_details_query.replace('{hash}', str(self.hash))
            account_details_query = account_details_query.replace('{network_id}', str(self.network_id))

            self.sql_client.command(account_details_query)
            self.sql_client.commit()
        else:
            logging.error('Missing username or password. Network: %s List ID: %s', self.name, str(self.list_id))
            return False

        return True
    def get_contact_by_pushuserid(self, pushuserid):
        #check if have to login
        self.connect()

        push_url = ''
        if self.esp_config['pushtrack_status'] == 'prod':
            push_url = self.esp_config['pushtrack_domain_prod'] + self.esp_config['pushtrack_get_by_pushuserid']
        else:
            push_url = self.esp_config['pushtrack_domain_dev'] + self.esp_config['pushtrack_get_by_pushuserid']

        push_url = push_url.replace('{pushuserid}', pushuserid)

        # Sets the headers.
        headers_dict = dict()
        headers_dict['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        headers_dict['Authorization'] = 'Bearer '+str(self.api_key)
        headers_dict['Content-Type'] = 'application/json'
        headers = headers_dict
        response_data = requests.get(push_url, headers=headers, timeout=5)
        return response_data