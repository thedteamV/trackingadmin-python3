""" ListsManager.py """
import sys
import time
import json
import logging
import traceback
from DB.Sql import Sql
from ESP.Sendlane import Sendlane
from ESP.Ongage import Ongage
from ESP.PushTracker import PushTracker
from ESP.ActiveTrail import ActiveTrail
from ESP.SenderMatrix import SenderMatrix
from ESP.SendlaneForm import SendlaneForm
from ESP.SendGrid import SendGrid
from ESP.Emercury import Emercury
from ESP.EngageBay import EngageBay
from SMSSP.SmsEdge import SmsEdge
from tqdm import tqdm


class ListsManager(object):
    """ Lists manager class """

    def __init__(self, name='ListsManager', sql_client=None, config_file=None, esp_config_file=None):
        """ Constructor. """
        self.name = name
        self.sql_client = sql_client
        self.config_file = config_file
        self.esp_config_file = esp_config_file

        with open(self.esp_config_file, 'rb') as esp_json_file:
                self.esp_config = json.load(esp_json_file)
                esp_json_file.close()

        if sql_client is None:
            logging.error('ListsManager could not connect to sql - No sql client')
            return

        self.esp_lists_list = self.sql_client.list_query(self.esp_config['lists_data_query'], self.esp_config['lists_data_query_fields'])

        self.esp_lists = dict()

        for esp_list in self.esp_lists_list:
            if esp_list['network'] == 'Sendlane':
                self.esp_lists[str(esp_list['list_id'])] = Sendlane('Sendlane', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'Ongage':
                self.esp_lists[str(esp_list['list_id'])] = Ongage('Ongage', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'Activetrail':    
                self.esp_lists[str(esp_list['list_id'])] = ActiveTrail('Activetrail', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'SenderMatrix':    
                self.esp_lists[str(esp_list['list_id'])] = SenderMatrix('SenderMatrix', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'SendlaneForm':
                self.esp_lists[str(esp_list['list_id'])] = SendlaneForm('SendlaneForm', self.config_file, self.esp_config_file, esp_list['list_id'], esp_list['network_name'])
            elif esp_list['network'] == 'Sendgrid':
                self.esp_lists[str(esp_list['list_id'])] = SendGrid('Sendgrid', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'Emercury':
                self.esp_lists[str(esp_list['list_id'])] = Emercury('Emercury', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'SmsEdge':
                self.esp_lists[str(esp_list['list_id'])] = SmsEdge('SmsEdge', self.config_file, self.esp_config_file, esp_list['list_id'])
            elif esp_list['network'] == 'EngageBay':
                self.esp_lists[str(esp_list['list_id'])] = EngageBay('EngageBay', self.config_file, self.esp_config_file, esp_list['list_id'], esp_list['api_key'])
            else:
                logging.warning('Unidentified network in ESP lists. List ID: %s', str(esp_list['list_id']))            

        logging.info('Instances were created for %s lists', str(len(self.esp_lists_list)))

    def connect(self):
        """ Connects to all the lists. """
        success_count = 0

        for esp_list in self.esp_lists:
            success_count += self.esp_lists[esp_list].connect(self.esp_config, self.sql_client)

        logging.info('Connections established successfully to %s/%s lists', str(success_count), str(len(self.esp_lists_list)))


    def add_contacts(self, contacts_list):
        """ Add contacts list to the networks. """
        contact_lists = dict()

        for user in contacts_list:
            logging.info('Send conversion: {} to list_id {}'.format(user['conversion_id'], user['list_id']))
            if str(user['list_id']) not in contact_lists.keys():
                contact_lists[str(user['list_id'])] = list()

            contact = dict()
            contact['first_name'] = user['user_name']
            contact['last_name'] = ''
            contact['email'] = user['email']
            contact['phone'] = user['phone']
            contact['country_code'] = user['country_code']
            contact['conversion_id'] = user['conversion_id']
            contact['conv_type'] = user['conv_type']
            contact['paramvar_1'] = user['paramvar_1']
            contact['paramvar_4'] = user['paramvar_4']

            try:
                contact['product_id'] = user['product_id']
            except Exception as e:
                logging.error('No product ID. Conversions ID: ' + str(contact['conversion_id']))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                contact['product_id'] = 0

            try:
                contact['net_list_id'] = user['net_list_id']
            except Exception as e:
                logging.error('No net_list_id. Conversions ID: ' + str(contact['conversion_id']))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                contact['net_list_id'] = 0


            contact_lists[str(user['list_id'])].append(contact)

        for list_id in contact_lists.keys():
            try:
                esp_list = self.esp_lists[list_id]
                contacts_to_add = contact_lists[list_id]
                esp_list.add_contacts(contacts_to_add)
            except Exception as e:
                logging.error('Error adding contacts to list: ' + str(list_id))
                logging.error(str(e))
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                logging.error(repr(traceback.format_tb(exc_traceback)))
                continue


        