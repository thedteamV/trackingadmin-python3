""" ESPBase.py """
import time
import json
import logging
from DB.ConnectionHub import ConnectionHub

class ESPBase(object):
    """ ESP Base class """

    def __init__(self, name='ESPBase', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name

        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.config = None
        self.esp_config = None

        self.sql_client = None

        if self.config_file is None:
            logging.error(self.name + ' can not find config file: %s', self.name)
            exit()

        if self.esp_config_file is None:
            logging.error(self.name + ' can not find ESP config file: %s', self.name)
            exit()

        try:
            with open(self.config_file, 'rb') as json_file:
                self.config = json.load(json_file)
                json_file.close()

            with open(self.esp_config_file, 'rb') as json_file:
                self.esp_config = json.load(json_file)
                json_file.close()

            self.connection_hub = ConnectionHub(config_file='Config/local_config.json')
            self.connection_hub.init(["tracking_db"])
            self.sql_client = self.connection_hub.get("tracking_db")

            logging.info(self.name + ' is connected to sql: %s list ID: %s', self.name, list_id)
        except:
            logging.info(self.name + ' could not connect to sql: %s', self.name)


    def connect(self):
        """ ESP connection. """

    def add_contact(self, contact):
        """ Adds new contact to a given list. """

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """

    def delete_contact(self, contact):
        """ Delete contact from a given list. """
