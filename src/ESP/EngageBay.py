import logging
import requests
from ESP.ESPBase import ESPBase
import traceback
import logging

class EngageBay(ESPBase):
    """ SendGrid class """
    def __init__(self, name='EngageBay', config_file=None, esp_config_file=None, list_id=0, api_key=''):
        """ Constructor. """
        self.name = name
        self.api_key = api_key
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
            'accept': 'application/json',
            'authorization': self.api_key,
            'content-type': 'application/json'
        }

        data = {
            'properties': [
                {'name': 'name', 'value': str(contact['first_name']), 'type': 'SYSTEM'},
                {'name': 'last_name', 'value': str(contact['last_name']), 'type': 'SYSTEM'},
                {'name': 'email', 'value': str(contact['email']), 'type': 'SYSTEM'},
                {'name': 'country_code', 'value': str(contact['country_code']), 'type': 'CUSTOM', 'field_type': 'TEXT'},
            ]
        }

        try:
            logging.info('Create contact in EngageBay.')
            create_response = requests.post('https://app.engagebay.com/dev/api/panel/subscribers/subscriber',
                                          headers=headers, json=data, timeout=(None, 2))

            if create_response.status_code > 299:
                logging.error(
                    'Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s',
                    self.name, str(self.list_id), str(contact['email']), str(create_response.status_code))
                return False

            logging.info('Link contact with list EngageBay.')
            link_with_list_response = requests.post(
                'https://app.engagebay.com/dev/api/panel/contactlist/add-subscriber/{email}/{list_id}'
                    .replace('{list_id}', str(contact['net_list_id'])).replace('{email}', str(contact['email'])),
                                            headers=headers, json={}, timeout=(None, 2))

            if link_with_list_response.status_code > 299:
                logging.error(
                    'Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s',
                    self.name, str(self.list_id), str(contact['email']), str(link_with_list_response.status_code))
                return False

        except requests.exceptions.ReadTimeout:
            logging.info("Do not wait for response")
            return True

        logging.info(
            'Contact added. Network: %s List ID: %s User: %s ',
            self.name,
            str(self.list_id),
            str(contact['email'])
        )

        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        for contact in contacts_list:
            added = self.add_contact(contact)
            if added:
                logging.info('Contacts added successfully. Network: %s List ID: %s', self.name, str(self.list_id))
            else:
                logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s', self.name,
                              str(self.list_id), str(contact['email']))

        return True

    def update_contact(self, contact):
        """ Updates contact from a given list. """

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """

    def delete_contact(self, contact):
        """ Delete contact from a given list. """

    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        return True
