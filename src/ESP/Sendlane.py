""" Sendlane.py """
import logging
import json
import requests
from ESP.ESPBase import ESPBase

class Sendlane(ESPBase):
    """ Sendlane class """

    def __init__(self, name='Sendlane', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        #ESPBase.__init__(self, self.name, self.config_file, self.esp_config_file, self.list_id)

        self.api_key = None
        self.hash = None
        self.net_list_id = None
        self.account_name = None
        self.owner = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        add_contact_request = self.esp_config['sendlane_add_contact']
        add_contact_request = add_contact_request.replace('{api_key}', self.api_key)
        add_contact_request = add_contact_request.replace('{hash}', self.hash)
        add_contact_request = add_contact_request.replace('{list_id}', self.net_list_id)
        add_contact_request = add_contact_request.replace('{email}', contact['email'])
        add_contact_request = add_contact_request.replace('{first_name}', contact['first_name'])
        add_contact_request = add_contact_request.replace('{last_name}', contact['last_name'])
        add_contact_request = add_contact_request.replace('{phone}', contact['phone'])
        add_contact_request = add_contact_request.replace('{tag_id}', contact['tag_id'])
        add_contact_request = add_contact_request.replace('{tag_name}', contact['tag_name'])

        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
        response_data = requests.post(add_contact_request, headers=headers, timeout=5)
        
        if response_data.status_code != 200:
            logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
            return False

        if 'error' in response_data.keys():
            logging.error('Error adding contact to network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
            return False

        logging.info('Contact added successfuly. Network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))

        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        add_contacts_request = self.esp_config['sendlane_add_contacts']
        add_contacts_request = add_contacts_request.replace('{api_key}', self.api_key)
        add_contacts_request = add_contacts_request.replace('{hash}', self.hash)
        add_contacts_request = add_contacts_request.replace('{list_id}', self.net_list_id)

        contacts_string = ''
        for contact in contacts_list:
            if contact['last_name'] != '':
                contacts_string += str(contact['first_name']) + ' ' + contact['last_name'] + '<' + str(contact['email']) + '>,'
            else:
                contacts_string += str(contact['first_name']) + '<' + str(contact['email']) + '>,'
        contacts_string = contacts_string[:-1]

        add_contacts_request = add_contacts_request.replace('{emails_list}', contacts_string)

        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
        response_data = requests.post(add_contacts_request, headers=headers, timeout=5)

        if response_data.status_code != 200:
            logging.error('Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
            return False

        logging.info('Contacts added successfuly. Network: %s List ID: %s', self.name, str(self.list_id))

        return True
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """
        return self.add_contact(self.list_id, contact)

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """
        unsubscribe_contact_request = self.esp_config['sendlane_unsubscribe_contact']
        unsubscribe_contact_request = unsubscribe_contact_request.replace('{api_key}', self.api_key)
        unsubscribe_contact_request = unsubscribe_contact_request.replace('{hash}', self.hash)
        unsubscribe_contact_request = unsubscribe_contact_request.replace('{email}', contact['email'])

        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
        response_data = requests.post(unsubscribe_contact_request, headers=headers, timeout=5)
        
        if response_data.status_code != 200:
            logging.error('Could not unsubscribe contact to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
            return False

        if 'error' in response_data.keys():
            logging.error('Error unsubscribing contact to network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
            return False

        logging.info('Contact unsubscribed successfuly. Network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
        
        return True


    def delete_contact(self, contact):
        """ Delete contact from a given list. """
        delete_contact_request = self.esp_config['sendlane_delete_contact']
        delete_contact_request = delete_contact_request.replace('{api_key}', self.api_key)
        delete_contact_request = delete_contact_request.replace('{hash}', self.hash)
        delete_contact_request = delete_contact_request.replace('{list_id}', self.net_list_id)
        delete_contact_request = delete_contact_request.replace('{email}', contact['email'])

        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
        response_data = requests.post(delete_contact_request, headers=headers, timeout=5)
        
        if response_data.status_code != 200:
            logging.error('Could not deleting contact to the list in the network: %s List ID: %s User: %s Error code: %s', self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
            return False

        if 'error' in response_data.keys():
            logging.error('Error deleting contact to network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
            return False

        logging.info('Contact deleted successfuly. Network: %s List ID: %s User: %s Error: %s', self.name, str(self.list_id), str(contact['email']), str(response_data['error']))
        
        return True


    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.api_key is not None and self.hash is not None and self.net_list_id is not None:
            return True

        # Checks the DB for active connection.
        account_details_query = self.esp_config['account_details_query']
        account_details_query = account_details_query.replace('{list_id}', str(self.list_id))
        account_details_query_fields = self.esp_config['account_details_query_fields']
        
        try:
            account_details = self.sql_client.list_query(account_details_query, account_details_query_fields)[0]
        except Exception as e:
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            return False

        self.api_key = account_details['api_key']
        self.hash = account_details['hash']
        self.net_list_id = account_details['net_list_id']
        self.list_name = account_details['list_name']
        self.network_id = account_details['network_id']
        self.network_name = account_details['network_name']
        self.owner = account_details['owner']

        # In case we had all the details in the DB.
        if self.api_key is not None and self.api_key != '' and \
           self.hash is not None and self.hash != '' and \
           self.net_list_id is not None:
            return True

        # No active connection in the DB, try to get it from the network itself.
        if account_details['user'] != '' and account_details['pass'] != '' and \
           account_details['user'] is not None and account_details['pass'] is not None:
            connect_request = self.esp_config['sendlane_connect']
            connect_request = connect_request.replace('{email}', account_details['user'])
            connect_request = connect_request.replace('{password}', account_details['pass'])

            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
            response_data = requests.post(connect_request, headers=headers, timeout=5)
            
            if response_data.status_code != 200:
                logging.error('Could not retrieve api key and hash from network: %s List ID: %s Error code: %s', self.name, str(self.list_id), str(response_data.status_code))
                return False

            response = json.loads(response_data.text)

            self.api_key = response['api_key']
            self.hash = response['api_hash']

            # Updates the DB with the new details.
            account_details_query = self.esp_config['account_update_details']
            account_details_query = account_details_query.replace('{api_key}', str(self.api_key))
            account_details_query = account_details_query.replace('{hash}', str(self.hash))
            account_details_query = account_details_query.replace('{network_id}', str(self.network_id))

            self.sql_client.command(account_details_query)
            self.sql_client.commit()
        else:
            logging.error('Missing username or password. Network: %s List ID: %s', self.name, str(self.list_id))
            return False

        return True
