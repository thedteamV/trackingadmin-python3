""" SenderMatrix.py """
import logging
import json
import requests
from ESP.ESPBase import ESPBase

class SenderMatrix(ESPBase):
    """ SenderMatrix class """

    def __init__(self, name='SenderMatrix', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        #ESPBase.__init__(self, self.name, self.config_file, self.esp_config_file, self.list_id)

        self.user = None
        self.password = None
        self.api_key = None
        self.hash = None
        self.net_list_id = None
        self.account_name = None
        self.owner = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        add_contact_request = self.esp_config['sender_matrix_add_contact']
        
        if contact['conv_type'] == 'FTD':
            add_contact_request += '?type=deposit'
        elif contact['conv_type'] == 'Lead':
            add_contact_request += '?type=signup'
        elif contact['conv_type'] == 'Optin':
            add_contact_request += '?type=optin'
        else:
            return False

        # Sets the headers.
        headers = dict()

        # Sets the body.
        body = dict()
        body['EMAIL'] = str(contact['email'])
        body['GEO'] = str(contact['country_code'])
        body['SOURCE'] = 'ESPDist'
        body['AFF_ID'] = '0'
        body['LIST_UID'] = str(self.net_list_id)
        body['FNAME'] = str(contact['first_name'])
        body['LNAME'] = str(contact['last_name'])


        response_data = requests.post(add_contact_request, headers=headers, data=body, timeout=5)
        
        # Checks the response.
        if response_data.status_code != 200:
            return False
        
        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        contacts_sent = 0

        for contact in contacts_list:
            if contact['email'] is None or contact['email'] == '':
                continue

            if self.add_contact(contact):
                contacts_sent += 1

        logging.info('%d/%d contacts added successfuly. Network: %s List ID: %s', contacts_sent, len(contacts_list), self.name, str(self.list_id))

        return True
    
    def update_contact(self, contact):
        """ Updates contact from a given list. """
        return False

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """
        return False


    def delete_contact(self, list_id, contact):
        """ Delete contact from a given list. """
        return False


    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.net_list_id is not None:
            return True

        # Checks the DB for active connection.
        account_details_query = self.esp_config['account_details_query']
        account_details_query = account_details_query.replace('{list_id}', str(self.list_id))
        account_details_query_fields = self.esp_config['account_details_query_fields']
        
        try:
            account_details = self.sql_client.list_query(account_details_query, account_details_query_fields)[0]
        except:
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            return False

        self.user = account_details['user']
        self.password = account_details['pass']
        self.api_key = account_details['api_key']
        self.hash = account_details['hash']
        self.net_list_id = account_details['net_list_id']
        self.list_name = account_details['list_name']
        self.network_name = account_details['network_name']
        self.owner = account_details['owner']

        # In case we had all the details in the DB.
        if self.net_list_id is not None:
            return True

        logging.error('Error getting network data from DB. Network: %s List ID: %s', self.name, str(self.list_id))

        return False
