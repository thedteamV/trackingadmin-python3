from requests_toolbelt import MultipartEncoder
from ESP.ESPBase import ESPBase
import traceback
import requests
import datetime
import logging
import sys

class Emercury(ESPBase):
    """ SendGrid class """

    def __init__(self, name='Emercury', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        self.user_email = None
        self.api_key = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        try:
            logging.info('Sending data to Emercury. url: ' + self.esp_config['emercury_url'])
            xml = """<?xml version="1.0" encoding="utf-8" ?>
			    <request>
				    <method>UpdateSubscribers</method>
				    <user mail="{email}" API_key="{api_key}"></user>
				    <parameters>
					    <audience_id>{list_id}</audience_id>
							<date_format_id>1</date_format_id>
							<subscriber>
							    <optin_date>{optin_date}</optin_date>
								<email>{contact_mail}</email>
								<first_name>{contact_firstname}</first_name>
						        <last_name>{contact_lastname}</last_name>
						        <user_field_10>{country}</user_field_10>
							</subscriber>
						</parameters>
					</request>""" \
                .replace('{optin_date}', datetime.datetime.now().strftime("%m/%d/%Y")) \
                .replace('{email}', self.user_email) \
                .replace('{api_key}', self.api_key) \
                .replace('{list_id}', str(contact['net_list_id'])) \
                .replace('{contact_mail}', str(contact['email'])) \
                .replace('{contact_firstname}', str(contact['first_name'])) \
                .replace('{contact_lastname}', str(contact['last_name'])) \
                .replace('{country}', str(contact['country_code']))

            data = MultipartEncoder(fields={'request': xml})
            headers = {"Content-type": data.content_type}

            response_data = requests.post(self.esp_config['emercury_url'], data=data, headers=headers)

            if response_data.status_code != 200:
                logging.error(
                    'Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s',
                    self.name, str(self.list_id), str(contact['email']), str(response_data.status_code))
                return False

            if "<error>" in str(response_data.content):
                logging.error(str(response_data.content))
                logging.error('Could not add contacts: request error',)
                return False

        except Exception as e:
            logging.error(str(e))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return False

        logging.info(
            'Contact added. Network: %s List ID: %s User: %s ',
            self.name,
            str(self.list_id),
            str(contact['email'])
        )

        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        for contact in contacts_list:
            added = self.add_contact(contact)
            if added:
                logging.info('Contacts added successfully. Network: %s List ID: %s', self.name, str(self.list_id))
            else:
                logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s', self.name,
                              str(self.list_id), str(contact['email']))

        return True

    def update_contact(self, contact):
        """ Updates contact from a given list. """

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """

    def delete_contact(self, contact):
        """ Delete contact from a given list. """

    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.api_key is not None:
            return True

        # Checks the DB for active connection.
        query = "SELECT user, api_key FROM esp_dist.esp_networks WHERE name = '{name}'".replace('{name}', self.name)
        try:
            account_details = self.sql_client.list_query(query, ['user', 'api_key'])[0]
        except Exception as e:
            logging.error(e)
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return False

        self.user_email = account_details['user']
        self.api_key = account_details['api_key']
        return True

