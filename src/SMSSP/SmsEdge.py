import logging
import requests
from ESP.ESPBase import ESPBase
import traceback
import sys

class SmsEdge(ESPBase):
    """ SmsEdge class """
    def __init__(self, name='SmsEdge', config_file=None, esp_config_file=None, list_id=0):
        """ Constructor. """
        self.name = name
        self.config_file = config_file
        self.esp_config_file = esp_config_file
        self.list_id = list_id
        self.api_key = None

    def connect(self, esp_config, sql_client):
        """ ESP connection. """
        self.esp_config = esp_config
        self.sql_client = sql_client
        if not self.get_account_access():
            logging.error('Error connecting to network: %s List ID: %s', self.name, str(self.list_id))
            return 0

        logging.info('Connected to network: %s List ID: %s', self.name, str(self.list_id))
        return 1

    def add_contact(self, contact):
        """ Adds new contact to a given list. """
        url = self.esp_config['smsedge_add_contacts'] \
            .replace('{list_id}', str(contact['net_list_id'])) \
            .replace('{api_key}', str(self.api_key)) \
            .replace('{number}', str(contact['phone'])) \
            .replace('{first_name}', str(contact['first_name'])) \
            .replace('{last_name}', str(contact['last_name']))

        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        }

        try:
            response_data = requests.get(url, headers=headers, timeout=(None, 2))

            if response_data.status_code != 200:
                logging.error(
                    'SMSEDGE: Could not add contacts to the list in the network: %s List ID: %s User: %s Error code: %s',
                    self.name, str(self.list_id), str(contact['phone']), str(response_data.status_code))
                return False

        except requests.exceptions.ReadTimeout:
            logging.info("Do not wait for response")
            return True
        except Exception as e:
            logging.error(str(e))
            return False

        logging.info(
            'Contact added. Network: %s List ID: %s User: %s ',
            self.name,
            str(self.list_id),
            str(contact['phone'])
        )

        return True

    def add_contacts(self, contacts_list):
        """ Adds new list of contacts to a given list. """
        for contact in contacts_list:
            added = self.add_contact(contact)
            if added:
                logging.info('Contacts added successfully. Network: %s List ID: %s', self.name, str(self.list_id))
            else:
                logging.error('Could not add contact to the list in the network: %s List ID: %s User: %s', self.name,str(self.list_id), str(contact['email']))

        return True

    def update_contact(self, contact):
        """ Updates contact from a given list. """

    def unsubscribe_contact(self, contact):
        """ Unsubscribe contact. """

    def delete_contact(self, contact):
        """ Delete contact from a given list. """

    def get_account_access(self):
        """ retrieves the account parameters """
        # In case connection already happend.
        if self.api_key is not None:
            return True

        # Checks the DB for active connection.
        query = "SELECT api_key FROM esp_dist.esp_networks n JOIN esp_dist.esp_lists l ON l.network_id = n.id WHERE l.id = '{list_id}' LIMIT 1".replace('{list_id}', str(self.list_id))
        try:
            account_details = self.sql_client.list_query(query, ['api_key'])[0]
        except Exception as e:
            logging.error(e)
            logging.error('Could not get network details from DB: %s List ID: %s', self.name, str(self.list_id))
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logging.error(repr(traceback.format_tb(exc_traceback)))
            return False

        self.api_key = account_details['api_key']
        return True