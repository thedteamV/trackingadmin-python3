pipeline {
    parameters {
        booleanParam(
            name: 'PROD_DEPLOY',
            defaultValue: false,
            description: 'Are you sure ?')
        string (
            defaultValue: '',
            description: 'Please left field empty if you want deploy from master branch source code...',
            name : 'DOCKER_IMAGE_TAG')
    }
    environment {
        IMAGE_NAME = 'avronim/tracking-admin'
        GIT_COMMIT_AUTHOR = sh(returnStdout: true, script: 'git log -1 --pretty=format:"%ae"').trim()
        MAIL_RECIPIENTS = '$DEFAULT_RECIPIENTS' + ', ' + "${env.GIT_COMMIT_AUTHOR}"
        DEPLOY_STATUS = '-'
    }
    agent any
    // To be done, need to expose docker daemon
    // agent {
    //     docker { image 'node:15.2' }
    // }
    stages {
        stage('Build') {
            when {
                expression { params.DOCKER_IMAGE_TAG.isEmpty() }
            }
            steps {
                echo "Start Build!"
                sh """
                    docker build . -t ${IMAGE_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER} \
                    --build-arg GIT_COMMIT=$GIT_COMMIT --build-arg BRANCH=$GIT_BRANCH
                """
            }
        }
        stage('Test') {
            steps {
                echo "Some Test !"
                // sh """
                //     docker build . -t ${IMAGE_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER} \
                //     --target tester
                // """
            }
        }
        stage('Publish to DTR') {
            when {
                expression { params.DOCKER_IMAGE_TAG.isEmpty() }
            }
            steps {
                echo "Publish image !"
                sh """
                    docker push ${IMAGE_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}
                """
            }
        }
        stage('Deploy to Stage') {
            environment {
                GIT = credentials('82e13a9e-5957-43c4-9fb3-2fff8a1efc03')
            }
            when {
                branch 'development'
            }
            steps {
                script {
                    echo "Deploy To k8s STAGE !!!"
                withCredentials([file(credentialsId: 'k8s_stage', variable: 'KUBECONFIG')]) {
                        sh 'git clone -b development https://$GIT_USR:$GIT_PSW@bitbucket.org/thedteamV/electra-helm.git helm'
                        sh """
                            cd helm
                            sed -i 's/tag:.*\$/tag: ${env.BRANCH_NAME}.${env.BUILD_NUMBER}/g' tracking-admin/values.yaml
                        """
                        if (params.DOCKER_IMAGE_TAG.isEmpty()) {
                            //If Docker image tag param is empty deploy from master branch current build
                            sh "cd helm && helm upgrade --install --wait tracking-admin tracking-admin/ --set image.tag=${env.BRANCH_NAME}.${env.BUILD_NUMBER} --namespace tracking"
                        } else {
                            sh "cd helm && helm upgrade --install --wait tracking-admin tracking-admin/ --set image.tag=${params.DOCKER_IMAGE_TAG} --namespace tracking"
                        }
                        sh """
                            cd helm
                            git add --all && git commit -m "Updated tracking admin STAGE tag"
                            git pull && git push
                        """
                    }
                    DEPLOY_STATUS = "Deployed into STAGE !"
                }
            }
        }
        stage('Deploy to Prod') {
            environment {
                GIT = credentials('82e13a9e-5957-43c4-9fb3-2fff8a1efc03')
            }
            when {
                branch 'master'
                expression { params.PROD_DEPLOY == true }
            }
            steps {
                script {
                    echo "Deploy To k8s PROD !!!"
                withCredentials([file(credentialsId: 'k8s_prod', variable: 'KUBECONFIG')]) {
                        sh 'git clone -b development https://$GIT_USR:$GIT_PSW@bitbucket.org/thedteamV/electra-helm.git helm'
                        sh """
                            cd helm
                            sed -i 's/tag:.*\$/tag: ${env.BRANCH_NAME}.${env.BUILD_NUMBER}/g' tracking-admin/values-PROD.yaml
                        """
                        if (params.DOCKER_IMAGE_TAG.isEmpty()) {
                            //If Docker image tag param is empty deploy from master branch current build
                            sh "cd helm && helm upgrade --install --wait tracking-admin tracking-admin/ --set image.tag=${env.BRANCH_NAME}.${env.BUILD_NUMBER} -f tracking-admin/values-PROD.yaml --namespace tracking"
                        } else {
                            sh "cd helm && helm upgrade --install --wait tracking-admin tracking-admin/ --set image.tag=${params.DOCKER_IMAGE_TAG} -f tracking-admin/values-PROD.yaml --namespace tracking"
                        }
                        sh """
                            cd helm
                            git add --all && git commit -m "Updated tracking admin PROD tag"
                            git pull && git push
                        """
                    }
                    DEPLOY_STATUS = "Deployed into PROD ! Please check status !"
                }
            }
        }
    }
    post {
        always {
            echo 'Cleaning...'
            sh """
                docker rmi ${IMAGE_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER} --force || true
            """
            cleanWs()
        }
        success {
            mail to:"$MAIL_RECIPIENTS", subject:"SUCCESS: ${currentBuild.fullDisplayName}",
            body: "Pipeline: '${env.JOB_NAME} [${env.BUILD_NUMBER}]' was success. \n Image pushed: ${IMAGE_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER} \n Deployment: $DEPLOY_STATUS"
            // We can get mail from git and send mail to commiter....
        }
        failure {
            mail to:"$MAIL_RECIPIENTS", subject:"FAILURE: ${currentBuild.fullDisplayName}",
            body: "Pipeline failed"
            // TODO: attach build logs to mail
        }
    }
}